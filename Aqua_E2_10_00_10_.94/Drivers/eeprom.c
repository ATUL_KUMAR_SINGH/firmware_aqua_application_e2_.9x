/********************************************************************************************************************************
 * File name:	 	eeprom.c
 *
 * Attention:		Copyright 2013 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 1 Nov, 2013 by Sahil Saini
 *
 * Description: 	This module is used to perform read write operations on eeprom.
 *******************************************************************************************************************************/
   
/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_i2c.h"
#include "stm32f0xx_gpio.h"
#include "i2c.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_misc.h"
#include "eeprom.h"
#include "pin_config.h"
#include "std_periph_headers.h"

/********************************************************************************************************************************
 * Function name: 	void eeprom_read_byte(uint8_t slave_addr, uint16_t mem_addr, uint8_t *rxptr, uint16_t nob)
 *
 * Returns: 		None
 *
 * Arguments: 		uint8_t				slave_addr
 * 					uint16_t			mem_addr
 * 					uint8_t				*rxptr
 * 					uint16_t			nob
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	16 Oct, 2013
 *
 * Date modified:	30 Jan, 2015
 *
 * Description: 	This function is used to read bytes from EEPROM.
 *
 * Notes:
 *******************************************************************************************************************************/
void eeprom_read_byte(uint8_t slave_addr, uint16_t mem_addr, uint8_t *rxptr, uint16_t nob) {

	while(I2C_GetFlagStatus(I2C1, I2C_ISR_BUSY) != RESET);
	i2c_slave_addr = slave_addr;
	i2c_mem_addr_ptr = (uint8_t *)&mem_addr;
	i2c_data_ptr = rxptr;
	i2c_num_of_bytes = nob;
	i2c_op = I2C_READ;
	i2c_state = I2C_TRANSACT_START;

	I2C_MasterRequestConfig (I2C1, I2C_Direction_Transmitter);
	I2C_TransferHandling (I2C1, i2c_slave_addr, 2, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);

	while (i2c_state != I2C_TRANSACT_COMPLETE);
	/*Busy bit flag*/
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));
	/* Clear STOPF flag */
	I2C_ClearFlag(I2C1, I2C_ICR_STOPCF);
	i2c_state = I2C_IDLE;

}

/********************************************************************************************************************************
 * Function name: 	void eeprom_transfer_byte(uint8_t slave_addr, uint16_t mem_addr, uint8_t *data, uint16_t nob)
 *
 * Returns: 		None
 *
 * Arguments: 		uint8_t				slave_addr
 * 					uint16_t			mem_addr
 * 					uint8_t				*data
 * 					uint16_t			nob
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	16 Oct, 2013
 *
 * Date modified:	30 Jan, 2015
 *
 * Description: 	This function is used to transfer bytes to EEPROM.
 *
 * Notes:
 *******************************************************************************************************************************/
void eeprom_transfer_byte(uint8_t slave_addr, uint16_t mem_addr, uint8_t *txptr, uint16_t nob) {
	volatile uint32_t delay_var;
	while(I2C_GetFlagStatus(I2C1, I2C_ISR_BUSY) != RESET);
	WP_OFF;
	i2c_slave_addr = slave_addr;
	i2c_mem_addr_ptr = (uint8_t *)&mem_addr;
	i2c_data_ptr = txptr;
	i2c_num_of_bytes = nob;
	i2c_op = I2C_WRITE;
	i2c_state = I2C_TRANSACT_START;

	I2C_MasterRequestConfig (I2C1, I2C_Direction_Transmitter);
	I2C_TransferHandling (I2C1, i2c_slave_addr, nob+2, I2C_SoftEnd_Mode, I2C_Generate_Start_Write);

	while (i2c_state != I2C_TRANSACT_COMPLETE);
	/*Busy bit flag*/
	while(I2C_GetFlagStatus(I2C1, I2C_FLAG_BUSY));
	/* Clear STOPF flag */
	I2C_ClearFlag(I2C1, I2C_ICR_STOPCF);
	for (delay_var = 0; delay_var < 200000; delay_var++);
	WP_ON;
	i2c_state = I2C_IDLE;
}

/********************************************************************************************************************************
 * Function name: 	void eeprom_data_read_write(uint16_t mem_adr, uint8_t read_wr_bit, uint8_t * buff, uint16_t nob)
 *
 * Returns: 		None
 *
 * Arguments: 		uint16_t			mem_addr
 * 					uint8_t				read_wr_bit
 * 					uint8_t				*buff
 * 					uint16_t			nob
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	16 Oct, 2013
 *
 * Date modified:	6 Nov, 2013
 *
 * Description: 	This function is used to perform read & write functions on eeprom.
 *
 * Notes:
 *******************************************************************************************************************************/
void eeprom_data_read_write(uint16_t mem_adr, uint8_t read_wr_bit, uint8_t *buff, uint16_t nob){
	uint16_t tx_byte_cnt = 0, cnt = 0;
	uint16_t block_no = 0;
	uint16_t byte_count = 0;

	tx_byte_cnt = 0;
	while(tx_byte_cnt < nob){
		block_no = mem_adr/32;
		block_no += 1;														// to calculate end address

		byte_count = ((block_no * 32) - mem_adr);

		cnt = nob - tx_byte_cnt;

		if(cnt > byte_count)
			cnt = byte_count;

		if(!read_wr_bit)
		eeprom_transfer_byte(EEPROM_ADDR, mem_adr, buff, cnt);
		else if(read_wr_bit)
		eeprom_read_byte(EEPROM_ADDR, mem_adr, buff, cnt/*nob*/);

		tx_byte_cnt += cnt;
		mem_adr += cnt;
		buff += cnt;
	}
}

