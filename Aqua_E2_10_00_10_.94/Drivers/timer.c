#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_tim.h"
#include "stm32f0xx_misc.h"
#include "timer.h"
#include "packet.h"
#include "uart.h"
#include "port_mapping.h"
#include "registration.h"
#include "system.h"
#include "button_input.h"
#include "std_periph_headers.h"
#include "led.h"
#include "water_management.h"
#include "buzzer.h"
#include "schedule.h"




uint32_t PrescalerValue = 0, CCR1_Val = 0;
__IO uint64_t timer_value = 0; 
uint8_t display_led_pattern_flag;
uint16_t sensor_sec_count, sensor_monitoring, pump_monitoring, adc_monitoring, feedback_monitoring;


uint8_t volatile buzzer_behaviour_kill_indication_time;


extern volatile uint8_t	beep_once_flag;
extern volatile uint8_t	beep_2sec;
volatile uint8_t beep_2sec_counter;
//uint16_t buzzer_activation_counter;
//uint8_t buzzer_activation_flag;   //atul

extern volatile uint8_t wifi_module_count,wifi_module_flag_set, start_low_atcmd_pin;

volatile uint32_t last_system_notification_info;
extern uint32_t system_notification_info;

/*
**===========================================================================
**		variable declaration
**===========================================================================
*/
uint16_t sensor_sec_count, udp_sec_count_glbl;	// for counting 1 second timer 5
extern uint8_t chk_dry_run_oht_flag, chk_dry_run_ugt_flag,buzzer_indication_num, prev_buzzer_indication_num, buzzer_behavior[8];
//__IO uint64_t timer2_value = 0, timer3_value = 0, timer4_value = 0, timer5_value = 0, timer9_value = 0, timer10_value = 0, timer11_value = 0, timer12_value = 0, timer13_value = 0, timer14_value = 0, micro_sec_cnt = 0, minutes_cnt = 0,\
// milli_sec_cnt = 0, timer_value = 0, tub_cnt = 0, temp_chk_counter = 0, data = 0, clk_cntr = 0, uart_pkt_wait_cnt; 
extern uint16_t day_time_in_minutes_glbl,dry_run_oht_counter, dry_run_ugt_counter;
volatile uint32_t driver_time_out;
extern volatile uint8_t wdgReloadByTimer, ugt_pump_trigger_flag, ugt_pump_trigger_count,  oht_pump_trigger_flag, oht_pump_trigger_count;
extern uint8_t oht_key_debounce_flag, oht_key_debounce_counter, oht_switch_status_curr, oht_switch_status_prev, ugt_key_debounce_flag, ugt_key_debounce_counter, ugt_switch_status_curr, ugt_switch_status_prev, oamp_key_debounce_flag, oamp_key_debounce_counter, oamp_switch_status_curr, oamp_switch_status_prev, ip_get_flag_glbl;
uint8_t oht_switch_low_flag, oht_switch_low_count, ugt_switch_low_flag, ugt_switch_low_count, oamp_switch_low_flag, oamp_switch_low_count, one_second_flag, one_min_flag, ip_chk_count_glbl, set_schedule_flag;
//uint32_t  CCR1_Val = 0;
extern tank_schedule oht_schd, ugt_schd, buzzer_schd;
//extern uint32_t udp_broadcast_counter;
extern uint16_t pump_evt_num;
extern uint8_t packet_process_count, packet_process_flag, gui_packet_process_flag, adc_flag;

extern RTC_DateTypeDef date_obj, RTC_DateStruct;
extern RTC_TimeTypeDef RTC_TimeStructure;

extern volatile uint8_t usb_delay, usb_delay_flag;


extern volatile uint8_t StopMode_Measure_flag;
volatile uint8_t StopMode_Measure_count;

extern struct auto_task auto_task_info;

#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif

extern struct buzzer_switch_behavior buzzer_switch_info;
extern struct buzzer_interval 	buzzer_interval_info;

 volatile uint8_t notification_counter_oht;

uint8_t presence_sensor_return = 1;

volatile uint8_t wifi_socket_250_ms_count, wifi_socket_250_ms_flag;

/* External Variables ------------------------------------------------------------*/
/* Qos */
/* oamp */
/* ntw settings */

TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
TIM_OCInitTypeDef TIM_OCInitStructure;

void TIM1_CC_IRQHandler (void) {
	if (TIM_GetITStatus(TIM1, TIM_IT_CC1) != RESET) {
		TIM_ClearITPendingBit (TIM1, TIM_IT_CC1);
	}  
}




void TIM16_IRQHandler (void) {  	
	static uint8_t display_time_count;
	static uint32_t ugt_switch_count_ms, oht_switch_count_ms, oamp_switch_count_ms;
	uint8_t  case_sel = 0;

	if (TIM_GetITStatus (TIM16, TIM_IT_Update) != RESET) {
		
		StopMode_Measure_count++;
		if(StopMode_Measure_count >= 100){		   // 1000 ms
		 	StopMode_Measure_count = 0;
			StopMode_Measure_flag = 1;
		}
		
		
		
		display_time_count++;
		if(display_time_count >= 3){		   // 30 ms
		 	display_time_count = 0;
			display_led_pattern_flag = 1;
		}
		


		
		if(beep_once_flag) {                    //10ms * 100 = 1000 ms
			beep_2sec_counter++;
			if(beep_2sec_counter >= 99) {
				blow_buzzer(BUZZER_OFF);
				beep_2sec_counter = 0;
				beep_once_flag = 0;
			}
		}

		wifi_socket_250_ms_count++;
		if(wifi_socket_250_ms_count == 50) {
			wifi_socket_250_ms_flag = 1;
			wifi_socket_250_ms_count = 0;
		}

		/* check ugt switch debounce */
		if(ugt_key_debounce_flag == 1){			   /* when ugt pump switch press        */
			ugt_key_debounce_counter++;
			if(ugt_key_debounce_counter == 10){		 /* scan after 100 ms  */
				ugt_key_debounce_flag = 0;
				ugt_switch_count_ms = 0;
				if(!UGT_MOTOR_STATUS){
					ugt_switch_low_flag = 1;
					ugt_switch_low_count = 1;
				}
			}
		}
		/* if debouncing not occur then count the time how long ugt switch press*/
		if(ugt_switch_low_flag == 1){
			if(UGT_MOTOR_STATUS == 0){
				ugt_switch_count_ms++;
				if(ugt_switch_count_ms == 100){
					ugt_switch_low_count++;
					ugt_switch_count_ms = 0;
				}
			}
			else{
				if(ugt_switch_low_count >= 1)
					case_sel = 1;
				if(ugt_switch_low_count >= 4)
					case_sel = 2;
				ugt_switch_low_flag = 0;
				ugt_switch_low_count = 0;
				switch(case_sel){
					case 1 : 
					    pump_evt_num |= MANUAL_UGT_PUMP_TRIGGER;
						pump_monitoring = 1;
					break;

					case 2:
						 pump_evt_num |= FORCEFULLY_UGT_PUMP_TRIGGER;
						 pump_monitoring = 1;
					break;							

				}
			}
		}
		/* check oht switch debounce */
		if(oht_key_debounce_flag == 1){			   /* when ugt pump switch press        */
			oht_key_debounce_counter++;
			if(oht_key_debounce_counter == 10){		 /* scan after 100 ms  */
				oht_key_debounce_flag = 0;
				oht_switch_count_ms = 0;
				if(!OHT_MOTOR_STATUS){
					oht_switch_low_flag = 1;
					oht_switch_low_count = 1;
				}
			}
		}
		/* if debouncing not occur then count the time how long oht switch press*/
		if(oht_switch_low_flag == 1){
			if(OHT_MOTOR_STATUS == 0){
				oht_switch_count_ms++;
				if(oht_switch_count_ms == 100){
					oht_switch_low_count++;
					oht_switch_count_ms = 0;
				}
			}
			else{
				if(oht_switch_low_count >= 1)
					case_sel = 1;
				if(oht_switch_low_count >= 4)
					case_sel = 2;
				oht_switch_low_flag = 0;
				oht_switch_low_count = 0;
				switch(case_sel){
					case 1 : 
					pump_evt_num |= MANUAL_OHT_PUMP_TRIGGER;
					pump_monitoring = 1;
					break;

					case 2:
					pump_evt_num |= FORCEFULLY_OHT_PUMP_TRIGGER;
					pump_monitoring = 1;
					break;							

				}
			}
		}
		/* check reset switch debounce */
		if(oamp_key_debounce_flag == 1){			   /* when ugt pump switch press        */
			oamp_key_debounce_counter++;
			if(oamp_key_debounce_counter == 10){		 /* scan after 100 ms  */
				oamp_key_debounce_flag = 0;
				if(!READ_RESET_BUZZER_SWITCH){
					oamp_switch_low_flag = 1;
					oamp_switch_low_count = 1;
				}
			}
		}
		/* if debouncing not occur then count the time how long reset switch press*/
		if(oamp_switch_low_flag == 1){
			case_sel = 0;
			if(READ_RESET_BUZZER_SWITCH == 0){
				oamp_switch_count_ms++;
				if(oamp_switch_count_ms == 100){
					oamp_switch_low_count++;
					oamp_switch_count_ms = 0;
				}
			}
			else{
				if(oamp_switch_low_count >= 1 && oamp_switch_low_count < 3){
					oamp_switch_low_flag = 0;
					oamp_switch_low_count = 0;
			//		isr_evt_set (0x0001, tid_buzzer_switch);		 /* event set to trigger reset switch */
				}
//				else if(oamp_switch_low_count >= 3 && oamp_switch_low_count < 30){
//					oamp_switch_low_flag = 0;
//					oamp_switch_low_count = 0;
//					udp_broadcast_state	=	RESET_SWITCH;
//					udp_broadcast_flag = 1;
//					udp_broadcast_counter = 600;
//				}
			}
		
		}

		if(packet_process_flag == 1){
			if(packet_process_count++ >= 10){				//Packet process afetr 100 ms
				packet_process_count = 0;
				packet_process_flag = 0;
				gui_packet_process_flag = 1;	
			}
			
		}

	}

		
/***********************************************/
	TIM_ClearITPendingBit (TIM16, TIM_IT_Update);
/***********************************************/			
}
	
	
	


//void TIM2_IRQHandler (void) {  			  //compare mode
//	static uint8_t display_time_count;
//
//	if (TIM_GetITStatus (TIM2, TIM_IT_CC1) != RESET) {			
//	
//		
//		display_time_count++;
//		if(display_time_count >= 3){		   // 30 ms
//		 	display_time_count = 0;
//			display_led_pattern_flag = 1;
//
//		    
//		}
//		TIM_ClearITPendingBit (TIM2, TIM_IT_CC1);	
//		/***********************************************/
//		
//		/***********************************************/			
//	}			
//}


//void TIM2_IRQHandler (void) {  				   //delay tesing on oscilloscope
//	static uint8_t display_time_count;
//
//	if (TIM_GetITStatus (TIM2, TIM_IT_Update) != RESET) {			
//		display_time_count++;
//
//	if(display_time_count % 2){		   // 10 ms
//		GPIO_SetBits(LED_PORT, OHT_LED_RED);
//   		}
//	else
//		GPIO_ResetBits(LED_PORT, OHT_LED_RED);
//
//		TIM_ClearITPendingBit (TIM2, TIM_IT_Update);	
//		/***********************************************/
//		
//		/***********************************************/			
//	}			
//}





void TIM3_IRQHandler (void) 
{
	if (TIM_GetITStatus (TIM3, TIM_IT_Update) != RESET) {
    sensor_sec_count++;
		RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		  // read the RTC time structure
		RTC_GetDate(RTC_Format_BIN, &date_obj);					  // read the RTC date structure
		one_second_flag = 1;
//		if(ip_get_flag_glbl == 1)
//			udp_sec_count_glbl++;
//		if(udp_broadcast_flag){
//			switch(udp_broadcast_state){
//				case POWER_ON:
//				case RESET_SWITCH:
//				case SOFT_SWITCH:
//				case CHANGE_IP:
//					if(udp_broadcast_counter-- == 0){  		// 10 min	
//						udp_broadcast_flag = 0;
//						udp_broadcast_state = 0;
//						udp_broadcast_counter = 0;	
//					}
//				break;
//			}
//		}
		if(RTC_TimeStructure.RTC_Seconds == 0){
			one_min_flag = 1;
			if(day_time_in_minutes_glbl++ > 1440){
				day_time_in_minutes_glbl = 0;
				set_schedule_flag = 1;				// when day expires restore schedule table
			}
			#ifdef DRY_RUN_ENABLE
				if(chk_dry_run_oht_flag == 1){
					dry_run_oht_counter--;
					if(dry_run_oht_counter == 0 && auto_task_info.tsk_enable_flag[0] == 1){
						chk_dry_run_oht_flag = 0;
						presence_sensor_return = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_3);
							if(presence_sensor_return) {
								pump_evt_num |= DRY_RUN_OHT_TRIGGER;
								pump_monitoring = 1;
							}	
					}
				}
				if(chk_dry_run_ugt_flag == 1 && status.oht_pump_ptr->pump_state == 0 &&  auto_task_info.tsk_enable_flag[1] == 1){
					dry_run_ugt_counter--;
					if(dry_run_ugt_counter == 0){
						chk_dry_run_ugt_flag = 0;
						pump_evt_num |= DRY_RUN_UGT_TRIGGER;
					    pump_monitoring = 1;
					}
				}
			#endif

			if ((!oht_schd.schd_running_flag) && (check_schedule_window_active (&oht_schd) >  0)) {
				oht_schd.schd_running_flag = 1;
				pump_evt_num |= SCHEDULE_START_OHT_TRIGGER;
				pump_monitoring = 1;	
			}
			else if ((oht_schd.schd_running_flag) && (check_schedule_window_active (&oht_schd) ==  2)) {
				oht_schd.schd_running_flag = 2;
				pump_evt_num |= SCHEDULE_START_OHT_TRIGGER;
				pump_monitoring = 1;	
			}

			else if ((oht_schd.schd_running_flag) && (!check_schedule_window_active (&oht_schd))) {
				oht_schd.schd_running_flag = 0;
				pump_evt_num |= SCHEDULE_START_OHT_TRIGGER;
				pump_monitoring = 1;
				
			}

			if ((!ugt_schd.schd_running_flag) && check_schedule_window_active (&ugt_schd)) {
				ugt_schd.schd_running_flag = 1;	
				pump_evt_num |= SCHEDULE_START_UGT_TRIGGER;
				pump_monitoring = 1;
			}
			else if ((ugt_schd.schd_running_flag) && (!check_schedule_window_active (&ugt_schd))) {
				ugt_schd.schd_running_flag = 0;
				pump_evt_num |= SCHEDULE_START_UGT_TRIGGER;
				pump_monitoring = 1;	
			}

		}	  //rtc 
	
		if(sensor_sec_count == SENSOR_READ_TIME){		 //Read sensor status after 15 Sec 
			sensor_sec_count = 0;
			sensor_monitoring = 1;
		}
//		if((oamp_switch_low_count >= 60 && oht_switch_low_count >= 60) || (oamp_switch_low_count >= 60 && ugt_switch_low_count >= 60)){
//			oht_switch_low_count = 0;
//			oamp_switch_low_count = 0;
//			oht_switch_low_flag = 0;
//			oamp_switch_low_flag = 0;
//			ugt_switch_low_count = 0;
//			ugt_switch_low_flag = 0;
//			isr_evt_set (0x0004, tid_buzzer_switch);
//				
//		}
//		if(ugt_switch_low_count >= 120 && oht_switch_low_count >= 120){
//			oht_switch_low_count = 0;
//			ugt_switch_low_count = 0;
//			oht_switch_low_flag = 0;
//			ugt_switch_low_flag = 0;
//			isr_evt_set (0x0008, tid_buzzer_switch);
//		}
//		if(oamp_switch_low_count == 30 && ugt_switch_low_flag == 0 && oht_switch_low_flag == 0){
//			oamp_switch_low_count = 0;
//			oamp_switch_low_flag = 0;
//			isr_evt_set (0x0002, tid_buzzer_switch);
//		}
//		if(oht_pump_trigger_flag == 1){
//			oht_pump_trigger_count++;
//			if(oht_pump_trigger_count >= 3){
//				CLR_OHT_MOTOR;
//				oht_pump_trigger_count = 0;
//				oht_pump_trigger_flag = 0;
//			}
//		}
//		if(ugt_pump_trigger_flag == 1){
//			ugt_pump_trigger_count++;
//			if(ugt_pump_trigger_count >= 3){
//				CLR_UGT_MOTOR;
//				ugt_pump_trigger_count = 0;
//				ugt_pump_trigger_flag = 0;
//			}
//		}

	   	TIM_ClearITPendingBit (TIM3, TIM_IT_Update);
	}
	
}



//void TIM6_DAC_IRQHandler (void) {
//	if (TIM_GetITStatus (TIM6, TIM_IT_Update) != RESET) {
////		led_info.__100msFlag = 1;
//		TIM_ClearITPendingBit (TIM6, TIM_IT_Update);
//  	}
//}

//
//  void TIM14_IRQHandler (void) {
//	
//	if (TIM_GetITStatus (TIM14, TIM_IT_CC1) != RESET) {
//
//    	count++ ;
//	 	if(count>=counter1)
//		{	
//			timer_flag = 1;		
//			
//		}

//	if(GPIO_ReadOutputDataBit(GPIOA,GPIO_Pin_2)==0) {
//		   		GPIO_SetBits(GPIOA,GPIO_Pin_2 );
//			}
//			else   {
//	     		GPIO_ResetBits(GPIOA,GPIO_Pin_2 );
//			}
//
//		TIM_ClearITPendingBit(TIM14, TIM_IT_CC1);
//	}
//}

void TIM14_IRQHandler(void) {

	if (TIM_GetITStatus(TIM14, TIM_IT_Update) != RESET)	{

		adc_monitoring = 1;
		feedback_monitoring = 1;
	//	notification_counter_oht++;
		
////		if(beep_once_flag) {
////			beep_2sec --;
//////			if(!beep_2sec)
//////			buzzer_activation_flag = 0;
////		}
		
		
		if(start_low_atcmd_pin) {
			wifi_module_count++;
			if(wifi_module_count == 3) {
				wifi_module_flag_set = 1;
			}
	  }
		
		
		

	   	#ifdef BUZZER_BEHAVIOUR_ENABLE
				buzzer_behavior_select();
				if(buzzer_switch_info.buzzer_activate_flag == BUZZER_ON  && buzzer_interval_info.buzzer_snooze_flag == 0){	
					if(buzzer_indication_num){
						if(buzzer_behaviour_kill_indication_time <= 70) {
							if(buzzer_interval_info.interval == 0){
								blow_buzzer(ON);	 					// buzzer beep
								buzzer_interval_info.beep_time--;
								if(buzzer_interval_info.beep_time == 0){
								//	buzzer_activation_flag = 0;
									blow_buzzer(OFF); 					// buzzer off
									buzzer_interval_info.beep_time = buzzer_behavior[buzzer_indication_num * 2];
									buzzer_interval_info.interval = buzzer_behavior[(buzzer_indication_num*2) + 1];	
								}
							}
							else {
								buzzer_interval_info.interval--;
							}
							buzzer_behaviour_kill_indication_time++;
							if(buzzer_behaviour_kill_indication_time > 70) {
								blow_buzzer(OFF); 					// buzzer off
							}
								
					 }
//					 else {
//						buzzer_behaviour_kill_indication_num = 0;
//					 }
						
				 }	
				}
				else if(buzzer_interval_info.buzzer_snooze_flag == 1 && buzzer_switch_info.buzzer_activate_flag == BUZZER_ON){
					buzzer_interval_info.snooze_time++;
					if(buzzer_interval_info.snooze_time >= buzzer_switch_info.buzzer_snooze_dur){
						buzzer_switch_info.buzzer_activate_flag = BUZZER_ON;	
						buzzer_interval_info.buzzer_snooze_flag = 0;
						buzzer_interval_info.snooze_time = 0;
					}
				}

		#endif
		TIM_ClearITPendingBit(TIM14, TIM_IT_Update);
  	}

}
 		
                            



void TIM17_IRQHandler(void) {
	
//uint8_t buzzer_activation_flag;
static uint16_t toggle_count, toggle_flag;
	
	if(TIM_GetITStatus(TIM17, TIM_IT_Update) != RESET){
		
//		if(buzzer_activation_flag == 1){
//			 if(toggle_count++ >= 8){			//14	// 2.56 khz  ////15*8 = 120+120 = 240 //4.1 khz
//					GPIO_ToggleBits(GPIOB, GPIO_Pin_4);
//				  toggle_count = 0;
//			}
//		}
		
//	toggle_count++;                    //testing code
//	if(toggle_count == 8)
//	{
//		GPIO_ToggleBits(GPIOA, GPIO_Pin_3);
//		toggle_count = 0;
//	}

	TIM_ClearITPendingBit(TIM17, TIM_IT_Update);
	}

}


///************************************************************************//**
//				void TIMER_OC_Config(TIM_TypeDef* TIMx, uint8_t timer_type)
//*
//* @brief		This function enables the Timer3 Interrupt.
//*
//* @param		TIMx = Timer no. can be	TIM3, TIM4, TIM5
//*				timer_type can be TIME_MICRO_SEC, TIME_MILLI_SEC, TIME_SEC, TIME_MINUTE, TIME_HOUR.
//*
//* @returns		None
//*
//* @exception	None.
//*
//* @author		Sahil Saini
//* @date			23/05/13
//*
//* @note			
//****************************************************************************/
//void TIMER_OC_Config(TIM_TypeDef* TIMx, uint8_t timer_type){
//	if(timer_type == TIME_MICRO_SEC) {
//		CCR1_Val = 24;
//	} else if(timer_type == TIME_MILLI_SEC)	{
//		CCR1_Val = 2399;
//       // CCR1_Val = 4799;
//	} else if(timer_type == TIME_SEC) {
//		CCR1_Val = 47999;
//	} else if(timer_type == TIME_MINUTE) {
//		CCR1_Val = 1439940;
//	} else if(timer_type == TIME_HOUR) {
//		CCR1_Val = 86396400;
//	}
//	
//	TIM_DeInit(TIMx);
//	/* Time base configuration */										
//	TIM_TimeBaseStructure.TIM_Period = CCR1_Val ;				   		// Autoreload register value initilization
//	TIM_TimeBaseStructure.TIM_Prescaler = timer_value - 1;	
//	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
//	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_CenterAligned1;    //TIM_CounterMode_Up;
//	TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure);
//	
//	/* Output Compare Timing Mode configuration: Channel1 */
//	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing;
//	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
//	TIM_OCInitStructure.TIM_Pulse = CCR1_Val ;		 //<**************
//	TIM_OC1Init(TIMx, &TIM_OCInitStructure);
//	
//	TIM_OC1PreloadConfig(TIMx, TIM_OCPreload_Disable);
//	TIM_ARRPreloadConfig(TIMx, ENABLE); 
//
//	if (TIMx == TIM6)
//		TIM_ITConfig(TIMx, TIM_IT_Update , ENABLE);
//	else
//		/* TIM Interrupts enable */
//		TIM_ITConfig(TIMx, TIM_IT_CC1 , ENABLE);
//		
//	/* TIM5 enable counter */
////	TIM_Cmd(TIMx, ENABLE);
//}
//
///************************************************************************//**
//				void TIM_Interrupt_Config(void)
//*
//* @brief		This function enables the Timer Interrupt.
//*
//* @param		None
//*
//* @returns		None
//*
//* @exception	None.
//*
//* @author		Aman Deep & Uday
//* @date			22/05/12
//*
//* @note			
//****************************************************************************/
//void timer_nvic_config(TIM_TypeDef* TIMx){
//	NVIC_InitTypeDef NVIC_InitStructure;
//
//	if(TIMx == TIM1){
//		/* TIM2 clock enable */
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
//		
//		/* Enable the TIM3 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM1_CC_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/
//	if(TIMx == TIM2){
//		/* TIM2 clock enable */
//		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
//		
//		/* Enable the TIM3 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/
//	else if(TIMx == TIM3){
//		/* TIM3 clock enable */
//		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
//
//		/* Enable the TIM3 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/
//	else if(TIMx == TIM6){
//		/* TIM3 clock enable */
//		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
//
//		/* Enable the TIM3 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM6_DAC_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/	
//	else if(TIMx == TIM14){
//		/* TIM5 clock enable */
//		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);
//		
//		/* Enable the TIM12 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM14_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/   
//	else if(TIMx == TIM15){
//		/* TIM5 clock enable */
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM15, ENABLE);
//		
//		/* Enable the TIM12 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM15_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/   
//	else if(TIMx == TIM16){
//		/* TIM5 clock enable */
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM16, ENABLE);
//		
//		/* Enable the TIM12 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM16_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/   
//	else if(TIMx == TIM17){
//		/* TIM5 clock enable */
//		RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM17, ENABLE);
//		
//		/* Enable the TIM12 gloabal Interrupt */
//		NVIC_InitStructure.NVIC_IRQChannel = TIM17_IRQn;
//		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
//	}
//	/**********************************************************/   
//
//	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//	NVIC_Init(&NVIC_InitStructure);
//	
//	/* TIMx Interrupts enable */
//	TIM_ITConfig(TIMx, TIM_IT_CC1 , ENABLE);
//}
//
///************************************************************************//**
//				uint64_t configure_timer_value(uint32_t timer_value_recvd, uint8_t time_flag_rcvd)
//*
//* @brief		This function Configures the Timer Interrupt Value.
//*
//* @param		[1]timer_value_recvd : Resemble the time to be set accordingly to the time flag receieved.
//*				[2]time_flag_rcvd : Time recieved in required format.
//
//* @returns		None
//*
//* @exception	None.
//*
//* @author		Sahil Saini
//* @date			22/05/13
//*
//* @note			
//****************************************************************************/
//void configure_timer_value(uint32_t timer_value_recvd, uint8_t time_flag_rcvd){
//
//	switch(time_flag_rcvd){
//
//		case TIME_MICRO_SEC:
//			timer_value = (uint64_t)(timer_value_recvd);  						// Time in Micro Sec
//			break;
//		case TIME_MILLI_SEC:
//			timer_value = ((uint64_t)(timer_value_recvd)*10); 					// Time in Milli Sec
//			break;
//		case TIME_SEC:
//			timer_value = ((uint64_t)(timer_value_recvd)*1000); 				// Time in Sec
//			break;
//		case TIME_MINUTE:
//			timer_value = ((uint64_t)(timer_value_recvd)*1000);			  		// Time in Minutes
//			break;
//		case TIME_HOUR:
//			timer_value = ((uint64_t)(timer_value_recvd)*1000);	 				//Time in Hours
//			break;
//		default :
//		 	timer_value = 0;  	
//	}  
//}
///************************************************************************//**
//				void timer_init(uint8_t timer_no, uint8_t timer_type, uint64_t timer_val)
//*
//* @brief		This function Configures the Timer Interrupt Value.
//*
//* @param		[1]timer_no : initialized timer number
//*	
//* @returns		None
//*
//* @exception	None.
//*
//* @author		Aman Deep
//* @date			08/08/12
//*
//* @note			
//****************************************************************************/
//void timer_init(TIM_TypeDef* TIMx, uint8_t timer_type, uint16_t timer_val) {
//	configure_timer_value (timer_val, timer_type);
//	if (TIMx == TIM6)
//		timer_value *= 2;
//	timer_nvic_config(TIMx);
//	TIMER_OC_Config(TIMx, timer_type);
//}










/************************************************************************//**
    void TIMER_OC_Config(TIM_TypeDef* TIMx, uint8_t timer_type)
*
* @brief  This function enables the Timer3 Interrupt.
*
* @param  TIMx = Timer no. can be TIM3, TIM4, TIM5
*    timer_type can be TIME_MICRO_SEC, TIME_MILLI_SEC, TIME_SEC, TIME_MINUTE, TIME_HOUR.
*
* @returns  None
*
* @exception None.
*
* @author  Sahil Saini
* @date   23/05/13
*
* @note   
****************************************************************************/
void TIMER_OC_Config(TIM_TypeDef* TIMx, uint8_t timer_type, uint32_t timer_val){
 TIM_TypeDef timer_vl;
 uint32_t timer_calc;
 float temp;
 temp  = SystemCoreClock/1000000;
 temp = 1/temp;
 timer_calc = timer_val/temp;
   
 if(timer_type == TIME_MICRO_SEC) {
  temp  = SystemCoreClock/1000000;
  CCR1_Val = (SystemCoreClock/(temp*1000000));
 } 
 else if(timer_type == TIME_MILLI_SEC) {
  temp  = SystemCoreClock/1000000;
  CCR1_Val = (SystemCoreClock/(temp*1000));
  } 
 else if(timer_type == TIME_SEC) {
   temp  = SystemCoreClock/1000000;
   CCR1_Val = (SystemCoreClock/(temp*1));
  }


 TIM_DeInit(TIMx);
 /* Time base configuration */          
 TIM_TimeBaseStructure.TIM_Period =  timer_calc - 1;     //207;    // Autoreload register value initilization
 TIM_TimeBaseStructure.TIM_Prescaler = CCR1_Val - 1;    // 0; 
 TIM_TimeBaseStructure.TIM_ClockDivision = 0;
 TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;   //TIM_CounterMode_CenterAligned1;
// TIM_TimeBaseStructure.TIM_RepetitionCounter = 1;
 TIM_TimeBaseInit(TIMx, &TIM_TimeBaseStructure);
 
 /* TIM Interrupts enable */
  TIM_ITConfig(TIMx, TIM_IT_Update , ENABLE);
}
/************************************************************************//**
    void TIM_Interrupt_Config(void)
*
* @brief  This function enables the Timer Interrupt.
*
* @param  None
*
* @returns  None
*
* @exception None.
*
* @author  Aman Deep & Uday
* @date   22/05/12
*
* @note   
****************************************************************************/
void timer_nvic_config(TIM_TypeDef* TIMx){
 NVIC_InitTypeDef NVIC_InitStructure;
 if(TIMx == TIM1){
  /* TIM2 clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
  
  /* Enable the TIM3 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM1_CC_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
 }
 /**********************************************************/
// if(TIMx == TIM2){
//  /* TIM2 clock enable */
//  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
//  
//  /* Enable the TIM3 gloabal Interrupt */
//  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPriority =0;
// }
 /**********************************************************/
 else if(TIMx == TIM3){
  /* TIM3 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
  /* Enable the TIM3 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
 }
 /**********************************************************/
 else if(TIMx == TIM14){
  /* TIM5 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);
  
  /* Enable the TIM12 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM14_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
 }
 /**********************************************************/   
// else if(TIMx == TIM15){
//  /* TIM5 clock enable */
//  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM15, ENABLE);
//  
//  /* Enable the TIM12 gloabal Interrupt */
//  NVIC_InitStructure.NVIC_IRQChannel = TIM15_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
// }
 /**********************************************************/   
 else if(TIMx == TIM16){
  /* TIM5 clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM16, ENABLE);
  
  /* Enable the TIM16 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM16_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 1;
 }
 /**********************************************************/   
 else if(TIMx == TIM17){
  /* TIM5 clock enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM17, ENABLE);
  
  /* Enable the TIM12 gloabal Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = TIM17_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 2;
 }
 /**********************************************************/  
 NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
 NVIC_Init(&NVIC_InitStructure);
 
 /* TIMx Interrupts enable */
// TIM_ITConfig(TIMx, TIM_IT_CC1 , ENABLE);
}
/************************************************************************//**
    uint64_t configure_timer_value(uint32_t timer_value_recvd, uint8_t time_flag_rcvd)
*
* @brief  This function Configures the Timer Interrupt Value.
*
* @param  [1]timer_value_recvd : Resemble the time to be set accordingly to the time flag receieved.
*    [2]time_flag_rcvd : Time recieved in required format.
* @returns  None
*
* @exception None.
*
* @author  Sahil Saini
* @date   22/05/13
*
* @note   
****************************************************************************/
void configure_timer_value(uint32_t timer_value_recvd, uint8_t time_flag_rcvd){
 switch(time_flag_rcvd){
  case TIME_MICRO_SEC:
   timer_value = (uint64_t)(timer_value_recvd);        // Time in Micro Sec
   break;
  case TIME_MILLI_SEC:
   timer_value = ((uint64_t)(timer_value_recvd)*10);      // Time in Milli Sec
   break;
  case TIME_SEC:
   timer_value = ((uint64_t)(timer_value_recvd)*1000);     // Time in Sec
   break;
  case TIME_MINUTE:
   timer_value = ((uint64_t)(timer_value_recvd)*1000);       // Time in Minutes
   break;
  case TIME_HOUR:
   timer_value = ((uint64_t)(timer_value_recvd)*1000);      //Time in Hours
   break;
  default :
    timer_value = 0;   
 }  
}
/************************************************************************//**
    void timer_init(uint8_t timer_no, uint8_t timer_type, uint64_t timer_val)
*
* @brief  This function Configures the Timer Interrupt Value.
*
* @param  [1]timer_no : initialized timer number
* 
* @returns  None
*
* @exception None.
*
* @author  Aman Deep
* @date   08/08/12
*
* @note   
****************************************************************************/
void timer_init(TIM_TypeDef* TIMx, uint8_t timer_type, uint16_t timer_val) {
 timer_nvic_config(TIMx);
 TIMER_OC_Config(TIMx, timer_type, timer_val);
}
