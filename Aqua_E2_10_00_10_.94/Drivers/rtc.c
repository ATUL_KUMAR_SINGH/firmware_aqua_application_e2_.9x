

#include "rtc.h"
#include "stm32f0xx_rtc.h"
#include "stm32f0xx_conf.h"
#include "button_input.h"


//struct RTC_Set_Time_struct  RTC_Set_Time_structure; 
//RTC_TimeTypeDef  RTC_Get_Time_structure;
//RTC_DateTypeDef 	 RTC_Get_date_structure;

RTC_DateTypeDef date_obj, RTC_DateStruct;
RTC_TimeTypeDef RTC_TimeStructure;
uint8_t	 DOM, MONTH, DOW, rtc_value, rtc_counter, alarm_status;
uint16_t  YEAR;
RTC_AlarmTypeDef  RTC_AlarmStructure;
uint32_t AsynchPrediv = 0, SynchPrediv = 0;
extern button device_mode;
//extern uint8_t display_power_led_flag, sleep_wake_up;


/**
  * @brief  This function handles RTC Auto wake-up interrupt request.
  * @param  None
  * @retval None
  */



void read_rtc(void){
//	RTC_Get_Time_structure.RTC_Format = RTC_Format_BCD;

	RTC_GetTime(RTC_Format_BCD, (&RTC_TimeStructure));
	RTC_GetDate(RTC_Format_BCD, (&RTC_DateStruct));

}

void RTC_initialize(void)
{
	RTC_InitTypeDef RTC_InitStructure;
//	RTC_TimeTypeDef RTC_TimeStructure;
//	RTC_DateTypeDef RTC_DateStructure;

	RTC_Config();

	// Configure the RTC data register and RTC prescaler 
//    RTC_InitStructure.RTC_AsynchPrediv = AsynchPrediv;
//    RTC_InitStructure.RTC_SynchPrediv = SynchPrediv;
//    RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
//
//	RTC_Init(&RTC_InitStructure);
	read_rtc();
//	PWR_WakeUpPinCmd(PWR_WakeUpPin_1, ENABLE);
//	PWR_WakeUpPinCmd(PWR_WakeUpPin_2, ENABLE);
//	 RTC_ClearFlag(RTC_FLAG_ALRAF);
}

/*
void RTC_Config(void)
{
  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);		// Enable the PWR clock 

  PWR_BackupAccessCmd(ENABLE);	// Allow access to RTC 

  RCC_LSEConfig(RCC_LSE_ON);		// Enable the LSE OSC 

  // Wait till LSE is ready   
  while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
  {
  }

  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);		// Select the RTC Clock Source 

  SynchPrediv = 0xFF;
  AsynchPrediv = 0x7F;

  RCC_RTCCLKCmd(ENABLE);			// Enable the RTC Clock 		

  RTC_WaitForSynchro();			  // Wait for RTC APB registers synchronisation 

  // Enable The TimeStamp 
//  RTC_TimeStampCmd(RTC_TimeStampEdge_Falling, ENABLE);    
}
*/


/**
  * @brief  Configures the RTC clock source.
  * @param  None
  * @retval None
  */
static void RTC_Config(void)
{
  RTC_TimeTypeDef   RTC_TimeStructure;
  RTC_InitTypeDef   RTC_InitStructure;
  EXTI_InitTypeDef EXTI_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;
  
  /* RTC Configuration **********************************************************/ 
  /* Enable the PWR clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);
  
  /* Allow access to RTC */
  PWR_BackupAccessCmd(ENABLE);    		//use to set a new time atul

  /* Reset back up registers */
//  RCC_BackupResetCmd(ENABLE);	   		//use to set a new time atul
//  RCC_BackupResetCmd(DISABLE);     //use to set a new time
	

/*****************  LSE SECTION ************************************/  
  /* Enable the LSE */
  RCC_LSEConfig(RCC_LSE_ON);	//atul
	
	  /* Wait till LSE is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
  {}
  
  /* Select the RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

/******************* LSE END ************************************/


/******************************* LSI SECTION ************************/	

//	RCC_LSICmd(ENABLE);
//  
//  /* Wait till LSI is ready */
//  while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
//  {}
//  
//  /* Select the RTC Clock Source */
//  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);
		
/**********************LSI END **************************************/		
  
  /* Enable the RTC Clock */
  RCC_RTCCLKCmd(ENABLE);
 
 // RTC_DeInit(); 
  /* Wait for RTC APB registers synchronisation */
  RTC_WaitForSynchro();  
  
  /* Set RTC calendar clock to 1 HZ (1 second) */
//  RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
//  RTC_InitStructure.RTC_AsynchPrediv = 0x7F;
//  RTC_InitStructure.RTC_SynchPrediv = 0x0FF;


  	RTC_InitStructure.RTC_AsynchPrediv = 127;			 
  	RTC_InitStructure.RTC_SynchPrediv = 255;
  	RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
  	RTC_Init(&RTC_InitStructure);
  
//  if (RTC_Init(&RTC_InitStructure) == ERROR)
//  {
//    while(1);
//  }

  /* Set the time to 01h 00mn 00s AM */
//  RTC_TimeStructure.RTC_H12     = RTC_H12_AM;
//  RTC_TimeStructure.RTC_Hours   = 0x01;
//  RTC_TimeStructure.RTC_Minutes = 0x00;
//  RTC_TimeStructure.RTC_Seconds = 0x00;  
//  
//  RTC_SetTime(RTC_Format_BCD, &RTC_TimeStructure);
//    
  /* Configure EXTI line 17 (connected to the RTC Alarm event) */
  EXTI_ClearITPendingBit(EXTI_Line17);
  EXTI_InitStructure.EXTI_Line = EXTI_Line17;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);
  
  /* NVIC configuration */
  NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure); 
}


/**
  * @brief  Configures the RTC clock source.
  * @param  None
  * @retval None
  */
void RTC_AlarmConfig(void)
{  
//  RTC_TimeTypeDef   RTC_TimeStructure;
  

  RTC_WriteProtectionCmd(DISABLE);
  RTC_AlarmCmd(RTC_Alarm_A, DISABLE);

  /* Get current time */
  RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);

  /* Set the alarm to current time + 5s */
  RTC_AlarmStructure.RTC_AlarmTime.RTC_H12     = RTC_H12_AM;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours   = RTC_TimeStructure.RTC_Hours;

  rtc_value = RTC_TimeStructure.RTC_Minutes;
  rtc_value /= 15;

  if(rtc_value == 0)
  {
//  	RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = RTC_TimeStructure.RTC_Minutes + 1;
   
	RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 15;
	rtc_counter++;
  }
  else if(rtc_value == 1)
  {
  	
  	RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 30;
	rtc_counter++;
  }	
  else if(rtc_value == 2)
  {
  	
  	RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 45;
	rtc_counter++;
  }		
  else if(rtc_value == 3)
  {
  	RTC_AlarmStructure.RTC_AlarmTime.RTC_Hours++;
  	RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = 00;
	rtc_counter++;
  }
  	
//  RTC_AlarmStructure.RTC_AlarmTime.RTC_Minutes = RTC_TimeStructure.RTC_Minutes + 3;
  RTC_AlarmStructure.RTC_AlarmTime.RTC_Seconds = 0;//RTC_TimeStructure.RTC_Seconds;
  RTC_AlarmStructure.RTC_AlarmDateWeekDay = 31;
  RTC_AlarmStructure.RTC_AlarmDateWeekDaySel = RTC_AlarmDateWeekDaySel_Date;
  RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_DateWeekDay;// | RTC_AlarmMask_Seconds |
                                    //RTC_AlarmMask_Hours;
  RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);
   
  /* Enable the RTC Alarm A interrupt */
  RTC_ITConfig(RTC_IT_ALRA, ENABLE);

  /* Enable the alarm */
  RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
  
  
  RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
    
  /* Clear the Alarm A Pending Bit */
  RTC_ClearITPendingBit(RTC_IT_ALRA);    
}


void RTC_IRQHandler(void)
{
  if (RTC_GetITStatus(RTC_IT_ALRA) != RESET)
  {
    /* LED4 on */
  //  STM_EVAL_LEDOn(LED4);
    /* Disable the RTC Alarm interrupt */
    RTC_ITConfig(RTC_IT_ALRA, DISABLE);
    RTC_AlarmCmd(RTC_Alarm_A, DISABLE);

//	 		SYSCLKConfig_STOP();
//			RTC_AlarmConfig();
	//	SystemInit();
//		device_mode.device_mode_state = ON;
//		adc_init();
//		display_power_led_flag = 1;
//		sleep_wake_up = 1;
		alarm_status =1;
    /* Clear the Alarm A Pending Bit */
    RTC_ClearITPendingBit(RTC_IT_ALRA);
    
    /* Clear EXTI line17 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line17);    
  }  
}



///************************************************************************//**
//*						void calc_dow(void)
//*
//* @brief				This routine is used to calculate day of week from the entered date.
//*
//* @param 				None.
//*
//* @returns				none.
//*
//* @exception			None.
//*
//* @author				Aman deep.
//* @date					15/03/12
//* @note					None.    
//****************************************************************************/


//void calc_dow(void){
//uint16_t year_val = 0, pending_days = 0;
//uint8_t leap_year = 0, month_odd_days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}, odd_days = 0, leap_odd_yr = 0 ;
//uint8_t chk_month = 0, temp_var = 0;
//year_val = YEAR;

///**********************For Leap Year **************************/

//	if((year_val % 4 == 0)&&(year_val % 100 != 0)||(year_val % 400 == 0))
//		leap_year = 1;
//	else
//		leap_year = 0;

///*********************Odd Days for 2000 Years******************/

//	if(year_val > 2000){
//		year_val -= 2000;
//		odd_days = 0;
//	}

///*****Calculate Odd days for Pending Years from 2000 Years*****/

//	year_val -= 1;
//	leap_odd_yr = (year_val / 4); 
//	year_val -= leap_odd_yr;
//	leap_odd_yr *= 2; 
//	odd_days = year_val + leap_odd_yr;

///**********Calculate Odd days for Pending Months****************/

//	chk_month = MONTH;								  	
//	for(temp_var = 0; temp_var < (chk_month-1); temp_var++){
//		pending_days += month_odd_days[temp_var];
//	}
//	if(MONTH > 2){
//		pending_days += (leap_year + DOM);
//	}
//	else 
//		pending_days += DOM;

///************Calculate Odd days for Pending Days*****************/

//	pending_days %= 7;
//	odd_days += pending_days;
//	if(odd_days > 7){
//		odd_days %= 7;
//	}
//	DOW = odd_days;
//	//if(DOW != 0)
////	DOW += 1;
//}


///************************************************************************//**
//*						void write_RTC(uint8_t hour, uint8_t min, uint8_t sec)
//*
//* @brief				This routine is used to write RTC Time.
//*
//* @param 				None.
//*
//* @returns				none.
//*
//* @exception			None.
//*
//* @author				Nikhil & Aman.
//* @date					15/03/12
//* @note					None.    
//****************************************************************************/

//void write_RTC(uint8_t *rtc_ptr, uint16_t year){
//	
//	
//	RTC_TimeStructure.RTC_Seconds = *rtc_ptr;	
//	RTC_TimeStructure.RTC_Minutes = *(rtc_ptr + 1);
//	RTC_TimeStructure.RTC_Hours = *(rtc_ptr + 2);
//	RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure);

//	
//	DOM      = *(rtc_ptr + 3);
//	MONTH 	 = *(rtc_ptr + 4);
//	YEAR  	 = year;
//	calc_dow();

//	date_obj.RTC_Month = MONTH;
//	date_obj.RTC_Date = DOM;
//	date_obj.RTC_Year = YEAR - 2000;//YEAR & 0xFF;//(uint8_t)YEAR;

//	RTC_DateStruct.RTC_Month = MONTH;
//	RTC_DateStruct.RTC_Date = DOM;
//	RTC_DateStruct.RTC_Year = YEAR - 2000;//(uint8_t)YEAR;
//	 
//	
//	switch(DOW){
//		case 0:
//			date_obj.RTC_WeekDay = RTC_Weekday_Sunday;
//			RTC_DateStruct.RTC_WeekDay = RTC_Weekday_Sunday;
//			break;
//		case 1:
//			date_obj.RTC_WeekDay = RTC_Weekday_Monday;
//			RTC_DateStruct.RTC_WeekDay = RTC_Weekday_Monday;
//			break;
//		case 2:
//			date_obj.RTC_WeekDay = RTC_Weekday_Tuesday;
//			RTC_DateStruct.RTC_WeekDay = RTC_Weekday_Tuesday;
//			break;
//		case 3:
//			date_obj.RTC_WeekDay = RTC_Weekday_Wednesday;
//			RTC_DateStruct.RTC_WeekDay = RTC_Weekday_Wednesday;
//			break;
//		case 4:
//			date_obj.RTC_WeekDay = RTC_Weekday_Thursday;
//			RTC_DateStruct.RTC_WeekDay = RTC_Weekday_Thursday;
//			break;
//		case 5:
//			date_obj.RTC_WeekDay = RTC_Weekday_Friday;
//			RTC_DateStruct.RTC_WeekDay = RTC_Weekday_Friday;
//			break;
//		case 6:
//			date_obj.RTC_WeekDay = RTC_Weekday_Saturday;
//			RTC_DateStruct.RTC_WeekDay = RTC_Weekday_Saturday;
//			break;
//		 default:
//		 	break;
//	}
//	RTC_SetDate(RTC_Format_BIN,&date_obj);
//	RTC_SetDate(RTC_Format_BIN,&RTC_DateStruct);
// 
//}
















