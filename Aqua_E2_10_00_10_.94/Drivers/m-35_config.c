/**************************************************************************************************
 * File name:	 	m-35_config.c
 *
 * Attention:		Copyright 2016 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 03 Oct, 2016 by Harmeen Kaur
 *
 * Description: 	This module is used to configure M-35 configure. 
 *************************************************************************************************/


/**************************************************************************************************
 * Include Section
 **************************************************************************************************/
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_syscfg.h"
#include "stm32f0xx_usart.h"
#include "uart.h"
#include "system.h"
#include "m-35_config.h"
#include "std_periph_headers.h"
#include "ascii_packet.h"
#include "packet.h"
//#include"tcp.h"

/**************************************************************************************************
* Defines section
 *************************************************************************************************/


/**************************************************************************************************
 * Global Variable Declaration Section.
 * AVOID DECLARING ANY.
 *************************************************************************************************/
uint8_t *string[22] =  {"at+WA=1","at+Assid=E4-Neotech","at+Assidl=10", "at+Aam=9", "at+Apw=12345678","at+Apwl=8", "at+Aip=192,168,0,99","at+mask=255,255,255,0","at+gw=192,168,0,99","at+UType=1", "at+UIp=192.168.0.99","at+ULPort=8080", "at+URPort=8080","at+SC=0","at+SC=1","at+SC=2","at+SC=3","at+SO=1,192,168,0,99,8080,8080", "at+SO=1,192,168,0,99,8080,8080","at+SO=1,192,168,0,99,8080,8080","at+SO=1,192,168,0,99,8080,8080","at+Rb=1" };
uint8_t *read_socket_commands[4]  = {"at+SR=0,1000", "at+SR=1,1000", "at+SR=2,1000", "at+SR=3,1000"};


//uint8_t *uart_pointer;
uint8_t **pointer_socket_value;
extern uint8_t char_in_string_count;
extern serial_port json_client;
//uint8_t array_temp[500];

 volatile uint8_t wifi_module_count,wifi_module_flag_set,polling_socket_flag, start_low_atcmd_pin;

  uint32_t len_atcmds;

/*
**===========================================================================
**		Function Prototype Section
**===========================================================================
*/
/* Forward declaration of functions. */

static void delay(uint32_t val)
{
	 uint32_t delay_count;
	 for(delay_count=0;delay_count<val;delay_count++);
}

/************************************************************************//**
* Function name: 	void m_35_module_config(void)
*
* @brief			This function configures M-35 module.

* param:			None
*
* @returns			none
*
* @exception		None.
*
* @author			Harmeen 
* @date				03 Oct 2016			
   
****************************************************************************/
void m_35_module_config(void)
{
 	uint8_t  ok_received_flag;
	uint8_t loop_count;
//	uint32_t len;
	
	for(loop_count = 0; loop_count < CONFIG_COMMAND; loop_count++)
	 {
	     ok_received_flag = 0;
		   len_atcmds = strlen((char *)string[loop_count]);
	  	 uart_send_str(USART2, string[loop_count],len_atcmds);
		   uart_send (USART2, 0x0D);		
			 
		 //char_in_string_count += 2;
//		 delay(3000000);
		 delay(2000000);
		 while(json_client.RxGetPtr != json_client.RxPutPtr)
		 { 
		 	if((*json_client.RxGetPtr == 'o') && (*(json_client.RxGetPtr+1) == 'k'))
			{
				
				if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN)){
					json_client.RxGetPtr = json_client.InBuf;
				}
			   json_client.RxGetPtr++;
			   ok_received_flag = 1;	
			}	
		 	else
			{
				if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN)){
					json_client.RxGetPtr = json_client.InBuf;
				}	
				json_client.RxGetPtr++;

			}
		 }
//		 if(ok_received_flag == 0)
//		 {
//		 	break;		 
//		 }
	  }  
}


 /************************************************************************//**
* Function name: 	void at_command_mode(void)
*
* @brief			This function configures ES/RST pin of M-35 module.

* param:			None
*
* @returns			none
*
* @exception		None.
*
* @author			Harmeen 
* @date				03 Oct 2016			
   
****************************************************************************/

void at_command_mode(void)
{
	GPIO_InitTypeDef   GPIO_InitStructure;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_Level_1;

  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;	
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  
}


/************************************************************************//**
* Function name: 	void wifi_enable(void)
*
* @brief			This function specifies that transfer of data will take place through wifi module.

* param:			None
*
* @returns			none
*
* @exception		None.
*
* @author			Harmeen 
* @date				03 Oct 2016			
   
****************************************************************************/

void wifi_enable(void)
{
   at_command_mode();

	 GPIO_ResetBits(GPIOB, GPIO_Pin_6);
	 start_low_atcmd_pin = 1;
	
	while(1)
	{
		if(wifi_module_flag_set)
		{
		 break;
		}									   	
	}
	
	GPIO_SetBits(GPIOB, GPIO_Pin_6);
	start_low_atcmd_pin = 0;
	
	m_35_module_config();
}








/************************************************************************//**
* Function name: 	void m_35_receive_config(void)
*
* @brief			This function recieves data through M-35 module and sends the response of packet back.

* param:			None
*
* @returns			none
*
* @exception		None.
*
* @author			Harmeen 
* @date				03 Oct 2016			
   
****************************************************************************/
void m_35_receive(uint8_t socket_poll_number)
{
  uint8_t flag = 0, *uart_pointer = 0, char_string = 0, local_array_index = 0, uart_value_check = 0, uart_value_check_1 = 0; 
  uint16_t bytes_count=0,parcel_length = 0;
	uint32_t len;
	
	
	len = strlen((char *)read_socket_commands[socket_poll_number]);
  uart_send_str(USART2, read_socket_commands[socket_poll_number],len);
  uart_send (USART2, 0x0D);
//  delay(200000);
//  uart_pointer = json_client.RxGetPtr;
//  char_string = strlen(read_socket_commands[socket_poll_number]);
//	
//	

//	if(strncmp(uart_pointer, read_socket_commands[socket_poll_number],char_string) == 0)
//	{
//				if(uart_pointer == (json_client.InBuf + RX_BUF_LEN))
//				{
//					uart_pointer = json_client.InBuf;
//				}	
//				while(json_client.RxGetPtr != json_client.RxPutPtr)
//				 { 	
//				 	if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//					{
//						json_client.RxGetPtr = json_client.InBuf;
//					}
//				 	uart_value_check  = *json_client.RxGetPtr;
//					json_client.RxGetPtr++;
//					if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//					{
//						json_client.RxGetPtr = json_client.InBuf;
//					}
//					uart_value_check_1 = *json_client.RxGetPtr;
//				   	json_client.RxGetPtr++;	 
//					if((uart_value_check == 0x0D) && (uart_value_check_1 == 0x0A))
//					{
//			
//					   if(json_client.RxGetPtr != json_client.RxPutPtr)
//						{
//							flag = 1;
//							break;
//						}
//					}		
//				 }

//				if(flag)
//				 {
//					  uint8_t local_array[200] = {0};
//					  local_array_index = 0;
//				  	while(json_client.RxGetPtr!= json_client.RxPutPtr)
//					{
//						  
//				  		if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//							{
//								  json_client.RxGetPtr = json_client.InBuf;
//							}
//							if((*json_client.RxGetPtr == ':'))
//							{
//							   if(json_client.RxGetPtr != json_client.RxPutPtr)
//								{
//									json_client.RxGetPtr++;
//									break;
//								}
//							}
//						bytes_count++;
//						local_array[local_array_index] = *json_client.RxGetPtr;
//						json_client.RxGetPtr++;
//						local_array_index++;
//					}
//			
//					  	if((local_array[0] == 'e') && (local_array[1] == 'r') && (local_array[2] == 'r'))
//						{
//							while(json_client.RxGetPtr != json_client.RxPutPtr)
//							{
//							 		if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//									{
//										json_client.RxGetPtr = json_client.InBuf;
//									}
//									else
//									{
//										json_client.RxGetPtr++;
//									}		
//							}				


//						}
//						else if(local_array[0] == '0')
//						{
//						 	while(json_client.RxGetPtr != json_client.RxPutPtr)
//							{
//							 		if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//									{
//										json_client.RxGetPtr = json_client.InBuf;
//									}
//									else
//									{
//										json_client.RxGetPtr++;
//									}		
//							}
//			
//						}
//						else
//						{
//						 uint8_t number_of_packets = 0, buff_check = 0; 
//						 parcel_length = ascii_decimal((uint8_t *)&local_array[0], bytes_count);
//						 uart_pointer = json_client.RxGetPtr;
//						 while(buff_check <= parcel_length)
//						 {
//						  	if(*uart_pointer == 0x0D)
//							{
//							  number_of_packets++;
//							}
//						 	uart_pointer++;
//							buff_check++;
//						 }
//						 for(buff_check = 0; buff_check <=number_of_packets;buff_check++)
//						 {
//						// packet_process();
//							json_client.RxGetPtr++;
//						 }
//						}	
//				}   				
//	}
//	else
//	{
//	  	while(json_client.RxGetPtr != json_client.RxPutPtr)
//		{
//		 		if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//				{
//					json_client.RxGetPtr = json_client.InBuf;
//				}
//				else
//				{
//					json_client.RxGetPtr++;
//				}		
//		}
//	}

}


