/************************************************************************//**
* @file			sensor.c
*
* @brief		This module contains the main WMS Ver 0.3.0.0 Application.
*
* @attention	Copyright 2014 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 12/04/14 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			this file contains the API for calculate current level of tank, check sensor
*				sensor malfunctioning and save sensor settings.
****************************************************************************/
#include<stm32f0xx.h>
#include "string.h"
//#include "misc.h"
#include "uart.h"
//#include "ethernet_packet.h"
#include "activateModules.h"
#include "target.h"
#include "water_management.h"
#include "buzzer.h"
#include "bitops.h"
#include "sensor.h"
//#include "client_registration.h"
//#include "display_panel.h"
//#include "stm32f2xx_rcc.h"
//#include "stm32f2xx_gpio.h"
//#include "led.h"
//#include "AT45DB161D.h"
//#include "virtual_sensor.h"
//#include "automated_tasks.h"
#include"json_client.h"
#include "memory_map.h"
//#include "logs.h"
#include <stm32f0xx.h>
#include "schedule.h"
#include "eeprom.h"
/*
**===========================================================================
**		variables declaration
**===========================================================================
*/
uint8_t average_level,*oht_flow_ptr, *ugt_flow_ptr;
uint32_t sensor_switch_input_bits_curr, sensor_switch_input_bits_prev;
extern uint8_t ugt_tank_calibrated, ugt_flow_rate_present, ugt_virtual_enable_gui_flag, oht_time_bw_sensor[10],ugt_time_bw_sensor[8];
extern uint32_t oamp_led_state_machine;
extern uint8_t json_send_arr[1000], *json_send_ptr;
extern uint32_t oht_inflow_rate, ugt_inflow_rate;
extern uint32_t oht_notification_info, ugt_notification_info;
extern uint16_t dry_run_ugt_counter, dry_run_oht_counter;


extern struct auto_task auto_task_info;
/*
**===========================================================================
**		structure declaration
**===========================================================================
*/
//extern struct tank_info OHT_tank, UGT_tank;
extern struct wms_sys_info wms_sys_config;
extern struct backup_info wms_bckup_config;
struct loc_tank_trigger_flags trigger_state;
//extern RTC_DateTypeDef date_obj;
//extern RTC_TimeTypeDef RTC_TimeStructure;
extern struct real_sensor_info  oht_real_sensor_config[TOTAL_OHT_TANK], ugt_real_sensor_config;
extern struct tank_setting oht_tank_config[TOTAL_OHT_TANK], ugt_tank_config;
#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif

/*
**===========================================================================
**		Global variable description
**===========================================================================
* 	average_level					-	average water level of configured tanks.
*	sensor_switch_input_bits_curr	-	holds the updated sensor port pin status.
*	sensor_switch_input_bits_prev	-	holds the sensor port pin status.
*/


/*===========================================================================
**
**		Function Section
**===========================================================================*/
/************************************************************************//**
*		       void read_pin_status()
*
* @brief		This function reads the sensor pin status.
*
* param                 None
*
*Return                 1 - on change on sensor pin status
*						0 - otherwise
*
* @author			Nikhil Kukreja
* @date				06/07/14
* @note			
****************************************************************************/
 
uint8_t read_pin_status(void){
	
	uint16_t new_status = 0;
	uint16_t port_stat_loc = 0;
	

		port_stat_loc = READ_OHT_L1;
		if(port_stat_loc)
			new_status |= 1<<0;
		else
			new_status &= ~(1<<0);
			
		port_stat_loc = READ_OHT_L2;
		if(port_stat_loc)
			new_status |= 1<<1;
		else
			new_status &= ~(1<<1);
		
		port_stat_loc = READ_OHT_L3;
		if(port_stat_loc)
			new_status |= 1<<2;
		else
			new_status &= ~(1<<2);
		
		port_stat_loc = READ_OHT_L4;
		if(port_stat_loc)
			new_status |= 1<<3;
		else
			new_status &= ~(1<<3);
	
	
//	port_stat_loc = READ_OHT_L5;
//	if(port_stat_loc)
//		new_status |= 1<<4;
//	else
//		new_status &= ~(1<<4);
	
//	port_stat_loc = READ_OHT_L6;
//	if(port_stat_loc)
//		new_status |= 1<<5;
//	else
//		new_status &= ~(1<<5);
//	
//	port_stat_loc = READ_OHT_L7;
//	if(port_stat_loc)
//		new_status |= 1<<6;
//	else
//		new_status &= ~(1<<6);
	
//	port_stat_loc = READ_OHT_L8;  //atul
//	if(port_stat_loc)
//		new_status |= 1<<7;
//	else
//		new_status &= ~(1<<7);
	
//	port_stat_loc = READ_OHT_L9;
//	if(port_stat_loc)
//		new_status |= 1<<8;
//	else
//		new_status &= ~(1<<8);
	
//	port_stat_loc = READ_OHT_L10;    //atul
//	if(port_stat_loc)
//		new_status |= 1<<9;
//	else
//		new_status &= ~(1<<9);
		 
	port_stat_loc = new_status;

	
	/*
		PE0	 = BIT 0			Sensor 1 OHT
		PE1	 = BIT 1			Sensor 2 OHT
		PE2	 = BIT 2			Sensor 3 OHT
		PE3	 = BIT 3			Sensor 4 OHT
		PE4	 = BIT 4			Sensor 5 OHT
		PE5	 = BIT 5			Sensor 6 OHT
		PE6	 = BIT 6			Sensor 7 OHT
		PE7	 = BIT 7			Sensor 8 OHT
		PE8	 = BIT 8			Sensor 9 OHT
		PE9	 = BIT 9			Sensor 10 OHT
		PE10 = BIT 10			Sensor 1 UGT
		PE11 = BIT 11			Sensor 2 UGT
		PE12 = BIT 12			Sensor 3 UGT
		PE13 = BIT 13			Sensor 4 UGT
		PE14 = BIT 14			Sensor 5 UGT
		PE15 = BIT 15			Sensor 6 UGT
			= BIT 16			Sensor 7 UGT
			= BIT 17			Sensor 8 UGT
//		PD11 = BIT 18			OHT PUMP(1) SWITCH
//		PB5 = BIT 19			UGT PUMP(2) SWITCH
//		PB14 = BIT 20			BUZZER SWITCH

	*/
	sensor_switch_input_bits_curr = port_stat_loc;
	if(sensor_switch_input_bits_curr != sensor_switch_input_bits_prev){		/* if any change occurs on previous port status & current port status */
		sensor_switch_input_bits_prev = sensor_switch_input_bits_curr;
		return 1;
	}
	else{
		return 0;
	}
}

/************************************************************************//**
*		       void power_sense_pin_init()
*
* @brief		This function initialize the power pin.
*
* param                 None
*
*Return                 None
*
* @author			Nikhil Kukreja
* @date				06/07/14
* @note			
****************************************************************************/



/************************************************************************//**
*		        void check_malfunctioning()
*
* @brief		This function check for malfunctioning of sensor.
*
* param                 None
*
*Return                 None
*
* @author			Nikhil Kukreja
* @date				06/05/14
* @note			
****************************************************************************/
uint8_t check_malfunctioning(struct tank_info *tank_ptr){
	uint8_t i_loc, j_loc, failure_flag = 0;

	tank_ptr->real_sensor_ptr->malfunctioning_sensor_bits = 0;
 	for(i_loc = tank_ptr->real_sensor_ptr->total_sensor ; i_loc > 0; i_loc--){	  
   		if((tank_ptr->real_sensor_ptr->sensor_status_bits >> (i_loc - 1) & 0x01) == 1){
     		for(j_loc = i_loc - 1; j_loc > 0; j_loc--){
       			if((tank_ptr->real_sensor_ptr->sensor_status_bits >> (j_loc - 1) & 0x01) == 0){
         			tank_ptr->real_sensor_ptr->malfunctioning_sensor_bits |= (1 << (j_loc - 1)); 
					failure_flag = 1;
				}
     		}
   		}
 	}

	return failure_flag;
}

/************************************************************************//**
*		        void get_current_level(void)
*
* @brief		this function get the current level of both tank and check for malfunctioning.
*
* param                 None
*
*Return     1 - when oht level change
*						2 - when ugt level change
*						3 - when both level change
*						0 - otherwise
*
* @author			Nikhil Kukreja
* @date				06/05/14
* @note			
****************************************************************************/
uint32_t get_current_level(struct tank_info *tank_ptr){

	uint8_t loc = 0, highest_sensor_num = 0;
	uint32_t ret = 0, oamp_flag;
	uint16_t temp_status = 0, temp_val; //, prev_status = 0;

	tank_ptr->real_sensor_ptr->prev_sensor_status_bits = tank_ptr->real_sensor_ptr->sensor_status_bits;
	
	tank_ptr->real_sensor_ptr->sensor_status_bits = 0;
	temp_val = sensor_switch_input_bits_curr >> (tank_ptr->real_sensor_ptr->port_num[0] - 1);
	oamp_flag = tank_ptr->tank_config_ptr->tank_num + 1;

	for(loc = 0;  loc < tank_ptr->real_sensor_ptr->total_sensor; loc++){
		if(((temp_val >> loc) & 0x01) == 0)
			tank_ptr->real_sensor_ptr->sensor_status_bits|= 1 << loc; 
		else
			tank_ptr->real_sensor_ptr->sensor_status_bits &= ~(1 << loc);				 	

	}
	temp_status = tank_ptr->real_sensor_ptr->sensor_status_bits;
	/* calculate highest sensor number  */	
	for(loc = 0;  loc < tank_ptr->real_sensor_ptr->total_sensor; loc++){
		if(temp_status >> loc & 0x01 == 1)
			highest_sensor_num = loc + 1; 		
	}
	// for dry run check
	if(highest_sensor_num > tank_ptr->real_sensor_ptr->highest_set_sensor){
		if(tank_ptr->tank_config_ptr->tank_num >= 1 && tank_ptr->tank_config_ptr->tank_num < 26)
			dry_run_oht_counter = auto_task_info.tsk_1_preset_level;
		else
			dry_run_ugt_counter = auto_task_info.tsk_2_preset_level;
	}	
	tank_ptr->real_sensor_ptr->highest_set_sensor = highest_sensor_num;
	tank_ptr->real_sensor_ptr->prev_malfunctioning_sensor_bits = tank_ptr->real_sensor_ptr->malfunctioning_sensor_bits;
	/* check for malfunctioning */
	if(check_malfunctioning(tank_ptr) == 1){			/*	sensor not malfunctioning */
		oamp_led_state_machine |= 1<< oamp_flag; 
		if(tank_ptr->tank_config_ptr->tank_num >= 1 && tank_ptr->tank_config_ptr->tank_num < 26){
			trigger_state.oht_sensor_malfnctng_flag = 1;
		 	oht_notification_info |= 1 << SENSOR_MALFUNCTIONING; 
		}
		else if(tank_ptr->tank_config_ptr->tank_num >=  26 && tank_ptr->tank_config_ptr->tank_num <  50){
			trigger_state.ugt_sensor_malfnctng_flag = 1;
			ugt_notification_info |= 1 << SENSOR_MALFUNCTIONING; 
		}
		//	 temp_flag = 1;
		 ret |= 0x01;
	}
	else
		oamp_led_state_machine &= ~(1<< oamp_flag); 
		//oamp_led_state_machine = 0;
	
	/*	if sensors are equidistant then current level			*/
	if(tank_ptr->real_sensor_ptr->equidistant_flag == 0){
	 	tank_ptr->current_level 	= highest_sensor_num * tank_ptr->real_sensor_ptr->cap_bw_sensor_perc[0];
		if(highest_sensor_num == tank_ptr->real_sensor_ptr->total_sensor)
			tank_ptr->next_hit_level =  tank_ptr->current_level;
		else
			tank_ptr->next_hit_level	= (highest_sensor_num + 1) * tank_ptr->real_sensor_ptr->cap_bw_sensor_perc[0];
	}
	/*	if sensors are not equidistant then current level		*/
	else{
		tank_ptr->next_hit_level = 0;
		tank_ptr->current_level = 0;
		for(loc = 0;  loc < highest_sensor_num; loc++){	
			tank_ptr->current_level += tank_ptr->real_sensor_ptr->cap_bw_sensor_perc[loc];		
		}
		if(highest_sensor_num == tank_ptr->real_sensor_ptr->total_sensor)
			tank_ptr->next_hit_level =  tank_ptr->current_level;
		else{
			for(loc = 0;  loc <= highest_sensor_num + 1; loc++){	
				tank_ptr->next_hit_level +=tank_ptr->real_sensor_ptr->cap_bw_sensor_perc[loc];		
			}
		}
	}
	if(tank_ptr->current_level != tank_ptr->prev_level){
		ret |= 0x80000000;
		tank_ptr->prev_level = tank_ptr->current_level;		
	}
	if(tank_ptr->current_level > 90)
		tank_ptr->current_level = 100;	
	
	#ifdef VIRTUAL_SENSOR
		if(OHT_tank.current_level != OHT_tank.virtual_current_level){	   	/* calculate virtual level */
			chnge_perc = OHT_tank.current_level - OHT_tank.virtual_current_level;
			if(chnge_perc > 10 || chnge_perc > (char)-10){
				// create log
				// calibrate virtual system
				create_log_v_error();							  /* if error occur more than 10 perc*/
				calibrate_oht_virtual_system(0);						  /* calibrate virtual system with real values */
			}

		}
	#endif
	
	
	return ret;
}
//////////////////////

void  calculate_average_level(){


	if(wms_sys_config.total_ugt > 0){			   	/* calcluate the average level of water in configured tank */
		average_level =  (status.ugt_current_level + status.oht_current_level)/2;
	}
	else
		average_level = status.oht_current_level;

}
/************************************************************************//**
*			void sensor_inputs_init(void)
*
* @brief		This routine initialize the sensor inputs and their interrupts.
*
* @param		None
*
* @returns		None
*
* @exception	None.
*
* @author		Nikhil Kukreja
*
* @date			11/04/14
*
* @note			
****************************************************************************/
//void sensor_inputs_init(void){
//	GPIO_InitTypeDef GPIOstruct;
//
//	RCC_AHB1PeriphClockCmd (OHT_CLOCK_PORT, ENABLE);	
//	RCC_AHB1PeriphClockCmd (UGT_CLOCK_PORT, ENABLE);
//	
//	GPIOstruct.GPIO_Mode = GPIO_Mode_IN;
//	GPIOstruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
//	GPIOstruct.GPIO_Pin  = 	GPIO_Pin_All;			  	
//	GPIO_Init(OHT_PORT, &(GPIOstruct));	
//	printCli("\n\n\rInitializing Inputs...");
//	//sensor_interrupt_intialize();	// should enable for WMS v0.2 board
////	nuc_sensor_interrupt_intialize();	// should enable for nuc board
//}


/************************************************************************//**
*		void save_sensor_settings(struct gui_payload *wms_payload_info, uint8_t num_bytes)
*
* @brief		This routine saves the sensor settings comes from GUI.
*
* @param		struct gui_payload *wms_payload_info - pointer to the structure contains data.
*
*				num_bytes	- number of bytes.
*
* @returns		None
*
* @exception	None.
*
* @author		Nikhil Kukreja
*
* @date			11/04/14
*
* @note			
****************************************************************************/
void save_sensor_settings(struct json_struct *wms_payload_info, uint16_t num_bytes){
	uint8_t tank_num,  field_length, indx = 0, sensor_info[3], temp_arr[20], temp, level = 0;
	uint16_t loc = 0, parse_len = 0;
	uint8_t  *sensor_config_ptr; 
	struct tank_setting *tank_config_ptr; 
	struct tank_info *tank_info_ptr; 
	struct real_sensor_info *sensor_info_ptr; 

	for(temp = indx; temp < wms_sys_config.total_oht; temp++){   //atul
				OHT_tank[temp].tank_config_ptr->tank_state  = 0;	
	}
	
	while(parse_len < num_bytes){
		field_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[parse_len]);
		parse_len += (field_length + 2);					// extract " "
		tank_num = ascii_decimal(&temp_arr[0], field_length);									/* fetch the tank number */	

		if(tank_num >= 1 && tank_num <= 25){				// oht
			sensor_config_ptr 	=	(uint8_t *)&oht_real_sensor_config[indx];
			tank_config_ptr  	= 	(struct tank_setting  *)&oht_tank_config[indx];	
			tank_info_ptr 	 	=   (struct tank_info *)&OHT_tank[indx];
			
		}
		else if(tank_num >= 26 && tank_num <= 50){				// ugt
			sensor_config_ptr 	=	(uint8_t *)&ugt_real_sensor_config;
			tank_config_ptr  	= 	(struct tank_setting  *)&ugt_tank_config;	
			tank_info_ptr    	=   (struct tank_info *)&UGT_tank;
		}

	// extract tank info 		
		tank_config_ptr->tank_num = tank_num;
		field_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[parse_len]);
		parse_len += (field_length + 2);
		tank_config_ptr->tank_state = ascii_decimal(&temp_arr[0], field_length);	

		field_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[parse_len]);
		parse_len += (field_length + 2);
		memcpy((uint8_t*)&tank_config_ptr->tank_name[0], temp_arr, field_length);	// parse tank name
		tank_config_ptr->tank_name[field_length] = '\0';

		field_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[parse_len]);
		parse_len += (field_length + 2);
		tank_config_ptr->tank_volume = ascii_decimal(&temp_arr[0], field_length);	

	  // extract pump info
  		field_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[parse_len]);
		parse_len += (field_length + 2);
	  	tank_info_ptr->pump_select = ascii_decimal(&temp_arr[0], field_length);
		field_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[parse_len]);
		parse_len += (field_length + 2);
		tank_info_ptr->flow_rate = ascii_decimal(&temp_arr[0], field_length);

	  // extract sensor info
		for(loc = 0; loc < 3; loc++){			// extract pump info
			field_length = extract_json_field_val(&temp_arr[0], &wms_payload_info->data[parse_len]);
			parse_len += (field_length + 2);					// extract " "
			sensor_info[loc] = ascii_decimal(&temp_arr[0], field_length);		
		}
		*sensor_config_ptr  = (sensor_info[1] - sensor_info[0]) + 1;	 // extract total sensor
		*(sensor_config_ptr + 1) = sensor_info[2]; 						 // sensor present at bottom flag
		for(loc = 0; loc < *sensor_config_ptr; loc++){					  // extract port numbers
			*(sensor_config_ptr + 3 + loc)  =  sensor_info[0] + loc;	
		}
		indx++;
	}
	// write settings to memory
	if(tank_num >= 1 && tank_num <= 25){
		for(indx = 0; indx < TOTAL_OHT_TANK; indx++){
			sensor_info_ptr 	=	(struct real_sensor_info*)&oht_real_sensor_config[indx];
			tank_config_ptr  	= 	(struct tank_setting  *)&oht_tank_config[indx];	
			tank_info_ptr 	 	=   (struct tank_info *)&OHT_tank[indx];
			//write tank setting
			eeprom_data_read_write(OHT_SETTING_START_ADDR + indx*TANK_SETTING_LEN, WRITE_OP,(uint8_t *)&tank_config_ptr->tank_num, sizeof(OHT_tank[indx]));
		//	dflash_read_multiple_byte(OHT_SETTING_START_ADDR + indx*TANK_SETTING_LEN, (uint8_t *)&tank_config_ptr->tank_num, sizeof(OHT_tank[indx]));
			// write pump setting
			eeprom_data_read_write(OHT_SETTING_START_ADDR + sizeof(OHT_tank[indx]) +indx*TANK_SETTING_LEN, WRITE_OP,(uint8_t *)&tank_info_ptr->pump_select, 2);
		//	dflash_read_multiple_byte(OHT_SETTING_START_ADDR + sizeof(OHT_tank[indx]) + indx*TANK_SETTING_LEN, (uint8_t *)&tank_info_ptr->pump_select, 2);
			// write sensor setting
			sensor_info[0] = sensor_info_ptr->port_num[0];
			sensor_info[1] = sensor_info_ptr->port_num[sensor_info_ptr->total_sensor - 1];
			sensor_info[2] = sensor_info_ptr->equidistant_flag;
			eeprom_data_read_write(OHT_SETTING_START_ADDR + sizeof(OHT_tank[indx]) + 2 +indx*TANK_SETTING_LEN, WRITE_OP,(uint8_t *)&sensor_info, 3);
		}
	}
	else if(tank_num >= 26 && tank_num <= 50){
		sensor_info_ptr 	=	(struct real_sensor_info*)&ugt_real_sensor_config;
		tank_config_ptr  	= 	(struct tank_setting  *)&ugt_tank_config;	
		tank_info_ptr    	=   (struct tank_info *)&UGT_tank;
		//write tank setting
		eeprom_data_read_write(UGT_SETTING_START_ADDR , WRITE_OP, (uint8_t *)&tank_config_ptr->tank_num, sizeof(UGT_tank));
	//	dflash_read_multiple_byte(UGT_SETTING_START_ADDR, (uint8_t *)&tank_config_ptr->tank_num, sizeof(UGT_tank));
		// write pump setting
		eeprom_data_read_write(UGT_SETTING_START_ADDR + sizeof(UGT_tank) , WRITE_OP, (uint8_t *)&tank_info_ptr->pump_select, 2);
	//	dflash_read_multiple_byte(UGT_SETTING_START_ADDR + sizeof(UGT_tank), (uint8_t *)&tank_info_ptr->pump_select, 2);
		// write sensor setting
		sensor_info[0] = sensor_info_ptr->port_num[0];
		sensor_info[1] = sensor_info_ptr->port_num[sensor_info_ptr->total_sensor - 1];
		sensor_info[2] = sensor_info_ptr->equidistant_flag;
		eeprom_data_read_write(UGT_SETTING_START_ADDR + sizeof(UGT_tank) + 2, WRITE_OP, (uint8_t *)&sensor_info, 3);
		
	
	}
	tank_sensor_calibrate();
	// calculate water level
	for(indx = 0; indx < wms_sys_config.total_oht; indx++){								/* read real sensor */
		get_current_level(&OHT_tank[indx]);	
		level += OHT_tank[indx].current_level;
	}
	status.oht_current_level = (level/wms_sys_config.total_oht);
	if(wms_sys_config.total_ugt > 0){
		get_current_level(&UGT_tank);
		status.ugt_current_level =  UGT_tank.current_level;
	}
	calculate_average_level();
}

/************************************************************************//**
*		void read_tank_pump_settings(void)
*
* @brief		This routine reads the tank settings from memory.
*
* @param		None
*
* @returns		None
*
* @exception	None.
*
* @author		Nikhil Kukreja
*
* @date			11/04/14
*
* @note			
****************************************************************************/
void read_tank_pump_settings(void){
	uint8_t indx = 0, sensor_info[3], loc;
	uint8_t  *sensor_config_ptr; 
	struct tank_setting *tank_config_ptr; 
	struct tank_info *tank_info_ptr; 

	for(indx = 0; indx < TOTAL_OHT_TANK; indx++){
		sensor_config_ptr 	=	(uint8_t *)&oht_real_sensor_config[indx];
		tank_config_ptr  	= 	(struct tank_setting  *)&oht_tank_config[indx];	
		tank_info_ptr 	 	=   (struct tank_info *)&OHT_tank[indx];
		//read tank setting
		eeprom_data_read_write(OHT_SETTING_START_ADDR + indx*TANK_SETTING_LEN, READ_OP,(uint8_t *)&tank_config_ptr->tank_num, sizeof(OHT_tank[indx]));
		// read pump setting
		eeprom_data_read_write(OHT_SETTING_START_ADDR + sizeof(OHT_tank[indx]) + indx*TANK_SETTING_LEN, READ_OP, (uint8_t *)&tank_info_ptr->pump_select, 2);
		// read sensor setting
		eeprom_data_read_write(OHT_SETTING_START_ADDR + sizeof(OHT_tank[indx]) + 2 +indx*TANK_SETTING_LEN, READ_OP, (uint8_t *)&sensor_info, 3);
		*sensor_config_ptr  = (sensor_info[1] - sensor_info[0]) + 1;	 // extract total sensor
		*(sensor_config_ptr + 1) = sensor_info[2]; 						 // sensor present at bottom flag
		for(loc = 0; loc < *sensor_config_ptr; loc++){					  // extract port numbers
			*(sensor_config_ptr + 3 + loc)  =  sensor_info[0] + loc;	
		}
	}
	sensor_config_ptr 	=	(uint8_t *)&ugt_real_sensor_config;
	tank_config_ptr  	= 	(struct tank_setting  *)&ugt_tank_config;	
	tank_info_ptr    	=   (struct tank_info *)&UGT_tank;
	//read tank setting
	eeprom_data_read_write(UGT_SETTING_START_ADDR, READ_OP, (uint8_t *)&tank_config_ptr->tank_num, sizeof(UGT_tank));
	// read pump setting
	eeprom_data_read_write(UGT_SETTING_START_ADDR + sizeof(UGT_tank), READ_OP, (uint8_t *)&tank_info_ptr->pump_select, 2);
	// read sensor setting
	eeprom_data_read_write(UGT_SETTING_START_ADDR + sizeof(UGT_tank) + 2, READ_OP, (uint8_t *)&sensor_info, 3);
	*sensor_config_ptr  = (sensor_info[1] - sensor_info[0]) + 1;	 // extract total sensor
	*(sensor_config_ptr + 1) = sensor_info[2]; 						 // sensor present at bottom flag
	for(loc = 0; loc < *sensor_config_ptr; loc++){					  // extract port numbers
		*(sensor_config_ptr + 3 + loc)  =  sensor_info[0] + loc;	
	}
	tank_sensor_calibrate();
}

/************************************************************************//**
*		void write_def_tank_pump_settings(void)
*
* @brief		This routine writes the default tank settings, pump settings and sensor setting in memory.
*				by default: 1 OHT, 100 Litres, tank1, oht pump enable
* @param		None
*
* @returns		None
*
* @exception	None.
*
* @author		Nikhil Kukreja
*
* @date			11/04/14
*
* @note			
****************************************************************************/
void write_def_tank_pump_settings(void){

	uint8_t indx = 0, sensor_info[3];
	uint8_t tank_pump_setting_configured;
	struct real_sensor_info  *sensor_info_ptr; 
	struct tank_setting *tank_config_ptr; 
	struct tank_info *tank_info_ptr; 

	
	sensor_info_ptr 	=	(struct real_sensor_info*)&oht_real_sensor_config[0];
	tank_config_ptr  	= 	(struct tank_setting  *)&oht_tank_config[0];	
	tank_info_ptr 	 	=   (struct tank_info *)&OHT_tank[0];
	//Enable 1 OHT
	tank_config_ptr-> tank_num = 1;
	tank_config_ptr-> tank_state = 1;
	memcpy((uint8_t*)&tank_config_ptr->tank_name[0], "OHT\0", 6);
	tank_config_ptr->tank_volume = 100;
	tank_info_ptr->pump_select = 1;
	sensor_info[0] = 1;		// start sensor port number 1
	sensor_info[1] = 4;	   // end sensor port number 4
	sensor_info[2] = 0;	  // sensor equidistant flag
	//write tank setting
	eeprom_data_read_write(OHT_SETTING_START_ADDR, WRITE_OP, (uint8_t *)&tank_config_ptr->tank_num, sizeof(OHT_tank[indx]));
	// write pump setting
	eeprom_data_read_write(OHT_SETTING_START_ADDR + sizeof(OHT_tank[indx]), WRITE_OP, (uint8_t *)&tank_info_ptr->pump_select, 2);
	// write sensor setting
	eeprom_data_read_write(OHT_SETTING_START_ADDR + sizeof(OHT_tank[indx]) + 2, WRITE_OP, (uint8_t *)&sensor_info, 3);

	//disable all the OHT
	for(indx = 1; indx < TOTAL_OHT_TANK; indx++){
		memset((uint8_t *)&oht_real_sensor_config[indx], 0, sizeof(oht_real_sensor_config[indx]));
		memset((uint8_t *)&oht_tank_config[indx], 0, sizeof(oht_tank_config[indx]));
		memset((uint8_t *)&OHT_tank[indx], 0, sizeof(OHT_tank[indx]));
		sensor_info_ptr 	=	(struct real_sensor_info*)&oht_real_sensor_config[indx];
		tank_config_ptr  	= 	(struct tank_setting  *)&oht_tank_config[indx];	
		tank_info_ptr 	 	=   (struct tank_info *)&OHT_tank[indx];
		
		tank_config_ptr-> tank_state = 0;		// tank disable
		//write tank setting
		eeprom_data_read_write(OHT_SETTING_START_ADDR + indx*TANK_SETTING_LEN, WRITE_OP, (uint8_t *)&tank_config_ptr->tank_num, sizeof(OHT_tank[indx]));
		// write pump setting
		eeprom_data_read_write(OHT_SETTING_START_ADDR + sizeof(OHT_tank[indx]) +indx*TANK_SETTING_LEN, WRITE_OP, (uint8_t *)&tank_info_ptr->pump_select, 2);
		// write sensor setting
		sensor_info[0] = sensor_info_ptr->port_num[0];
		sensor_info[1] = sensor_info_ptr->port_num[sensor_info_ptr->total_sensor - 1];
		sensor_info[2] = sensor_info_ptr->equidistant_flag;
		eeprom_data_read_write(OHT_SETTING_START_ADDR + sizeof(OHT_tank[indx]) + 2 +indx*TANK_SETTING_LEN, WRITE_OP, (uint8_t *)&sensor_info, 3);
	}
	

	//disable UGT
	sensor_info_ptr 	=	(struct real_sensor_info*)&ugt_real_sensor_config;
	tank_config_ptr  	= 	(struct tank_setting  *)&ugt_tank_config;	
	tank_info_ptr    	=   (struct tank_info *)&UGT_tank;

	tank_config_ptr-> tank_state = 0;		// tank disable
	//write tank setting
	eeprom_data_read_write(UGT_SETTING_START_ADDR, WRITE_OP, (uint8_t *)&tank_config_ptr->tank_num, sizeof(UGT_tank));
//	dflash_read_multiple_byte(UGT_SETTING_START_ADDR, (uint8_t *)&tank_config_ptr->tank_num, sizeof(UGT_tank));
	// write pump setting
	eeprom_data_read_write(UGT_SETTING_START_ADDR + sizeof(UGT_tank), WRITE_OP, (uint8_t *)&tank_info_ptr->pump_select, 2);
//	dflash_read_multiple_byte(UGT_SETTING_START_ADDR + sizeof(UGT_tank), (uint8_t *)&tank_info_ptr->pump_select, 2);
	// write sensor setting
	sensor_info[0] = sensor_info_ptr->port_num[0];
	sensor_info[1] = sensor_info_ptr->port_num[sensor_info_ptr->total_sensor - 1];
	sensor_info[2] = sensor_info_ptr->equidistant_flag;
	eeprom_data_read_write(UGT_SETTING_START_ADDR + sizeof(UGT_tank) + 2,WRITE_OP, (uint8_t *)&sensor_info, 3);
	
	tank_pump_setting_configured = 1;
	eeprom_data_read_write(TANK_PUMP_SENSOR_SETTING_ADDR, WRITE_OP,&tank_pump_setting_configured, 1);
	

}
/* this API reset the local flags that has been set on sensor and motor trigger */
void reset_loc_flags(){		 
	
//	trigger_state.oht_level_change_flag		= 0;
	trigger_state.oht_motor_trigger_flag	= 0;
//	trigger_state.ugt_level_change_flag		= 0;
	trigger_state.ugt_motor_trigger_flag	= 0;
	trigger_state.oht_sensor_malfnctng_flag = 0;
	trigger_state.ugt_sensor_malfnctng_flag = 0;
}

/************************************************************************//**
*		void get_tank_status(struct tank_info *tank_ptr)
*
* @brief		This routine creates the json packet for passed tank number.

* @param		struct tank_info *tank_ptr - pointer to tank structure
*
* @returns		None
*
* @exception	None.
*
* @author		Nikhil Kukreja
*
* @date			11/04/15
*
* @note			
****************************************************************************/

//void get_tank_status(struct tank_info *tank_ptr){
//	uint8_t field_name_indx = 1, len;
//
// 
//	ADD_START_BRACE
//
//	add_field_name(field_name_indx++);				/* add tank number */
//	ADD_DOUBLE_QUOTE;
//	len = dec_ascii_arr(tank_ptr->tank_config_ptr->tank_num, json_send_ptr);
//	json_send_ptr += len; 
//	ADD_DOUBLE_QUOTE;
//	ADD_COMMA;
//
//	add_field_name(field_name_indx++);				/* add currnet level*/
//	ADD_DOUBLE_QUOTE;
//	dec_ascii_byte(tank_ptr->current_level, json_send_ptr, 3);		
//	json_send_ptr += 3;
//	ADD_DOUBLE_QUOTE;
//	ADD_COMMA;
//
//	add_field_name(field_name_indx++);				/* add virtual currnet level*/
//	ADD_DOUBLE_QUOTE;
//	dec_ascii_byte(tank_ptr->virtual_current_level, json_send_ptr, 3);		
//	json_send_ptr += 3;
//	ADD_DOUBLE_QUOTE;
//	
//	ADD_CLOSING_BRACE;
//}
//


/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/

