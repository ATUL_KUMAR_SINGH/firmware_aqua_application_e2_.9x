/************************************************************************//**
* @file			main.c
*
* @brief		This module contains the main Cool Master Application.
*
* @attention	Copyright 2015 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 22/12/15 \n by \Sonam
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			 
****************************************************************************/

/*
**===========================================================================
**		Include section
**===========================================================================
*/
#include <stdlib.h>
#include "stm32f0xx.h"
#include "std_periph_headers.h"
#include "timer.h"
#include "buzzer.h"
#include "water_management.h"
#include "client_registration.h"
#include "json_client.h"
#include "uart.h"
#include "schedule.h"
#include "adc.h"
#include "memory_map.h"
#include "json_client.h"
#include "buyers_n_client_reg.h"
#include "sensor.h"


/*
**===========================================================================
**		Defines section
**===========================================================================
*/

/*
**===========================================================================
**		Global Variable Declaration Section
**		AVOID DECLARING ANY
**===========================================================================
*/

extern uint16_t sensor_monitoring, pump_monitoring, adc_monitoring, feedback_monitoring;
extern uint8_t  oht_feedback_volt, ugt_feedback_volt;
extern struct tank_status status;
schedule_window oht_schedule_window [TOTAL_SCHEDULE], buzzer_schedule_window [TOTAL_SCHEDULE];	//, ugt_schedule_window [TOTAL_SCHEDULE]		//atul
extern uint8_t json_send_arr[1000], step_check_flag;
uint8_t  oht_pump_trigger_cause = 0, ugt_pump_trigger_cause = 0, gui_packet_process_flag, update_nw_settings;
uint8_t sendbuf[1000];	
uint32_t system_notification_info, oht_notification_info, ugt_notification_info, event_info;
extern struct loc_tank_trigger_flags trigger_state;
tank_schedule oht_schd, ugt_schd, buzzer_schd;
uint16_t day_time_in_minutes_glbl = 0;
uint16_t pump_evt_num, adc_evt_num;
extern uint32_t adc_channel_type;

#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif

extern serial_port json_client;
uint8_t license_type, OHT_triggered, error_flag_num = 0;  //UGT_triggered     //atul

uint8_t wifi_packet_process_flag;


extern struct tank_immediate_setting oht_immed, ugt_immed;

volatile uint8_t StopMode_Measure_flag;

extern RTC_TimeTypeDef RTC_TimeStructure;
extern RTC_DateTypeDef date_obj;
RTC_TimeTypeDef last_pump_triggered_time_OHT; //last_pump_triggered_time_UGT;    //atul
RTC_DateTypeDef last_pump_triggered_date_OHT; //last_pump_triggered_date_UGT;			//atul
uint8_t OHT_pump_state; 											// UGT_pump_state;									//atul
uint8_t adc_flag, oht_pump_wire_disconnect, ugt_pump_wire_disconnect;; 

volatile uint8_t vsense_result;
extern volatile uint8_t wifi_socket_250_ms_flag;

//volatile uint8_t notification_counter_oht;
extern volatile uint32_t last_system_notification_info;
//volatile uint8_t notification_flag;

uint8_t volatile usb_delay, usb_delay_flag; 



/*
**===========================================================================
**		Function Prototype Section
**===========================================================================
*/
/* Forward declaration of functions. */
extern uint8_t display_led_pattern_flag;
extern struct wms_sys_info wms_sys_config;
void tsk_sensor_trigger(void);
void tsk_pump_trigger(void);
void scan_periodic_tsk(void);


/**************************************************************************//*
*			  void tsk_Main(void)
*
* @brief		
*
* @author		Atul Kumar
* @date			22/12/15
****************************************************************************/
void main(void){
	  
	uint16_t JSON_rx_packet_status, len;
	static uint8_t power_count_flag;
	unsigned long volatile start;
	
	
	/* Wait for debugger connection 0.3s*/
	for (start = 0; start < 1000000; start++) { ; }
	for (start = 0; start < 1000000; start++) { ; }
	for (start = 0; start < 1000000; start++) { ; }
	for (start = 0; start < 1000000; start++) { ; }


	usbd_init();
	Init_Target ();			                          //Initialize all hardware peripherals 
	
	while(1){
		
		if(StopMode_Measure_flag == 1){	
			StopMode_Measure_flag = 0;
			vsense_result = GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_13);
			if(!vsense_result){  
				power_count_flag++;
				if(power_count_flag > 3){
					power_count_flag = 0;
					StopMode_Measure();			//call low power modes used for rtc		
				}
			}	
			else {
				power_count_flag = 0;	
			}				
		}
		
				
		if(display_led_pattern_flag){		      // Led Pattern Control flag
			display_led_pattern_oht();				  // display the LED pattern of OHT tank 
//			if(wms_sys_config.total_ugt > 0)
//				display_led_pattern_ugt();			 // display the LED pattern of UGT tank 
			
			display_led_pattern_oamp();				 // display the LED pattern of OAMP 
			display_led_pattern_flag = 0;
		}
		
		tsk_sensor_trigger();           //Sensor control (using DI) and display led pattern as per selected Sensor
		tsk_pump_trigger(); 	         //Motor control through Swith
		
								  
		if(gui_packet_process_flag) {			     //packet received on Wifi or USB
			while(json_client.RxGetPtr != json_client.RxPutPtr){	
				JSON_rx_packet_status =	JSON_server_packet_validates(&json_client);
				switch(JSON_rx_packet_status){
					case PACKET_VALIDATED_RESPONSE_BROADCAST:
					case PACKET_VALIDATED:
	
							len = strlen((char *)json_send_arr);
							memset(sendbuf,0x20,1000);
							strncpy((char *)sendbuf, (char *)json_send_arr, (len) );	 
//						uart_send_str(USART2, json_send_arr, len);
							usb_send_data(sendbuf, (len));
							for (start = 0; start < 500000; start++) { ; }
							
					break;
				}
			}
			gui_packet_process_flag = 0;
		}



		if(feedback_monitoring) {						//after every 1 sec call feedback monitoring
			scan_periodic_tsk();              //ADC Feedback task
			adc_channel_type = ADC_Channel_0;	//PUMP_OHT_ADC_CHN  scan after 3 Sec
			select_feedback_chnl();			
			feedback_monitoring = 0;
		}

	}
}






/************************************************************************//**
*				void tsk_sensor_trigger (void)
*
* @brief		This task wait for an sensor or switch trigger event.if sensor 
*				position change then calculate the level of tank.if motor switch 
*				press then toggle the state of motor.   
*		
*
* @author		Nikhil Kukreja
* @date			02/05/14
* @note			
****************************************************************************/

void tsk_sensor_trigger(void) { 
	uint8_t evt_num, *send_ptr, count, auto_tsk_status, temp, task_run, buff[20];
	static uint8_t timer_chk_flag = 0, timer_chk_counter = 0;
	uint16_t packet_len = 0, loc_var;
	uint32_t oht_tank_status[TOTAL_OHT_TANK] = {0}, ugt_tank_status = 0, level = 0;


//	send_ptr = sendbuf;

	/**********************************************************/
	/*					Wait for Time Based Event			  */
	/**********************************************************/
		
		if(sensor_monitoring) {		
		//	case SENSOR_MONITORING_10_SEC:				 		
				if(read_pin_status()){										/* read real sensor */
					timer_chk_flag = 1;
					timer_chk_counter = 0;							
				}
				else{						/* calculate the accurate level	*/
					if(timer_chk_flag == 1){
						if(timer_chk_counter == 0){
							level = 0;
							for(count = 0; count < wms_sys_config.total_oht; count++){		
								oht_tank_status[count] = get_current_level(&OHT_tank[count]);						// get oht level
								level += OHT_tank[count].current_level;
							}
							status.oht_current_level = (level/wms_sys_config.total_oht);
							if(wms_sys_config.total_ugt > 0){							 // if ugt present
								ugt_tank_status = get_current_level(&UGT_tank);							  // get ugt level
								status.ugt_current_level =  UGT_tank.current_level;
							}
							calculate_average_level();
							//scan automated task 	
							automated_task_process();
							// check pump status for oht pump
							if(scan_pump_status(OHT_tank[0].tank_config_ptr->tank_num) != 0){
						//		os_evt_set (AUTOMATIC_OHT_TRIGGER, tid_pump_evt);
								pump_evt_num |= AUTOMATIC_OHT_TRIGGER;
					   			pump_monitoring = 1;								
							}
							if(wms_sys_config.ugt_pump_select == 1){
								if(scan_pump_status(UGT_tank.tank_config_ptr->tank_num) != 0){
							//		os_evt_set (AUTOMATIC_UGT_TRIGGER, tid_pump_evt);
									pump_evt_num |= AUTOMATIC_UGT_TRIGGER;
					   				pump_monitoring = 1;								
								}
							}
														
							
		 				//	calculate_save_consumption();									/* save the water consumption if occur */
					
						//	create_GUI_tx_packet(GET_CURRENT_WATER_LEVEL_PUMP_STATUS, 0, 0);
						//	packet_len = strlen((char *)json_send_arr);
						//  send_data_json_server(json_send_arr);
							
					//		save_sensor_state(&buff[0]);
					//		create_GUI_tx_packet(SEND_EVENT_LOG, 0, &buff[0]);
					//		send_data_json_server(json_send_arr);

							reset_loc_flags();	
							timer_chk_flag = 0;
							timer_chk_counter = 0;

						}
						else{
							timer_chk_counter += SENSOR_READ_TIME;							
						}
					}
				}
		//		create_GUI_tx_packet(GET_CURRENT_WATER_LEVEL_PUMP_STATUS, 0, 0);  //15 sec send tank status to app
		//	  	packet_len = strlen((char *)json_send_arr);
		//		uart_send_str(USART1, json_send_arr, packet_len);
			  	sensor_monitoring = 0;
			}	
	
	
}





/************************************************************************//**
*				void tsk_sensor_trigger (void)
*
* @brief		This task wait for an sensor or switch trigger event.if sensor 
*				position change then calculate the level of tank.if motor switch 
*				press then toggle the state of motor.   
*		
*
* @author		Nikhil Kukreja
* @date			02/05/14
* @note			
****************************************************************************/
void tsk_pump_trigger(void) { 
	uint16_t evt_num;
	uint32_t oht_tank_status = 0, ugt_tank_status = 0;
	uint8_t auto_tsk_status, count, ret_val = 0, prev_oht_status, prev_ugt_status;
	uint8_t temp;
	uint16_t length;
	
	/**********************************************************/
	/*					Wait for Time Based Event			  */
	/**********************************************************/
	//	os_evt_wait_or(0xFFFF, 0xFFFF);
	//   	evt_num = os_evt_get ();
//		step_check_flag = 0;

	if(pump_monitoring) {	
		prev_oht_status = status.oht_pump_ptr->pump_state;
		prev_ugt_status = status.ugt_pump_ptr->pump_state;
		for(temp = 0; temp < 16; temp++){
			if((((pump_evt_num >> temp) & 0x01) == 0x01)){
				switch(temp){	  					
					case EVT_MANUAL_OHT_PUMP_TRIGGER:									 /* this event occurs when OHT pump manually trigger */ 
					case EVT_FORCEFULLY_OHT_PUMP_TRIGGER:							
					case EVT_DRY_RUN_OHT_TRIGGER:
					case EVT_AUTOMATIC_OHT_TRIGGER:
					//	if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
							if(((oht_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
								if(!(chk_exception_schedule(OHT_tank[0].tank_config_ptr->tank_num))){    //atul
								//	step_check_flag = 1;
									if(temp == EVT_AUTOMATIC_OHT_TRIGGER || temp == EVT_DRY_RUN_OHT_TRIGGER || temp == EVT_FORCEFULLY_OHT_PUMP_TRIGGER){ //|| temp == EVT_MANUAL_OHT_PUMP_TRIGGER){
									//	step_check_flag = 2;
										trigger_motor(OHT_tank[0].tank_config_ptr->tank_num);
										last_pump_triggered_time_OHT = RTC_TimeStructure;
										last_pump_triggered_date_OHT = date_obj;
										OHT_triggered = 1;
									//	OHT_pump_state = status.oht_pump_ptr->pump_state;
										trigger_state.pump_cause = SYSTEM_TRIGGER;
										event_info |= (1 << OHT_PUMP_TRIGGER); 
										if(temp == EVT_DRY_RUN_OHT_TRIGGER)
											oht_pump_trigger_cause = 1 << MOTOR_TRIGGER_FROM_DRY_RUN;	
										else if(temp == EVT_FORCEFULLY_OHT_PUMP_TRIGGER)	
											oht_pump_trigger_cause = 1 << MOTOR_TRIGGER_FROM_SWITCH;
										else
											oht_pump_trigger_cause = 1 << MOTOR_TRIGGER_FROM_AUTO_TASK;
									}
									else{
										if((ret_val = chk_automatic_task_status(OHT_tank[0].tank_config_ptr->tank_num, !(status.oht_pump_ptr->pump_state))) == 0){
										//	step_check_flag = 3;
											trigger_motor(OHT);
											last_pump_triggered_time_OHT = RTC_TimeStructure;
											last_pump_triggered_date_OHT = date_obj;
											OHT_triggered = 1;
										//	OHT_pump_state = status.oht_pump_ptr->pump_state;
											trigger_state.pump_cause = SYSTEM_TRIGGER;
											event_info |= (1 << OHT_PUMP_TRIGGER); 
											oht_pump_trigger_cause = 1 << MOTOR_TRIGGER_FROM_SWITCH;							 	
										}
										else{
										//	step_check_flag = 4;
											system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task
										  oht_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task
											notification_info_oht();										
										}
									} //atul
								}
							//	else;
									// due to exception schedule 
							}
							else {
								oht_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
								notification_info_oht();
						}  
//						else{
//							oht_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
//						}
					break;
						
//					case EVT_MANUAL_UGT_PUMP_TRIGGER:									 /* this event occurs when OHT pump manually trigger */ //atul
//					case EVT_FORCEFULLY_UGT_PUMP_TRIGGER:	
//					case EVT_DRY_RUN_UGT_TRIGGER:
//					case EVT_AUTOMATIC_UGT_TRIGGER:
//						if(wms_sys_config.ugt_pump_select == 1){
//						//	if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
//								if(((ugt_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
//							//		if(!(chk_exception_schedule(UGT_tank.tank_config_ptr->tank_num))){
//										if(temp == EVT_AUTOMATIC_UGT_TRIGGER || temp == EVT_DRY_RUN_UGT_TRIGGER|| temp == EVT_FORCEFULLY_UGT_PUMP_TRIGGER){
//											trigger_motor(UGT_tank.tank_config_ptr->tank_num);
//											last_pump_triggered_time_UGT = RTC_TimeStructure;
//											last_pump_triggered_date_UGT = date_obj;
//											UGT_triggered = 1;
//										//	UGT_pump_state = status.ugt_pump_ptr->pump_state;
//											event_info |= (1 << UGT_PUMP_TRIGGER); 
//											if(temp == EVT_DRY_RUN_UGT_TRIGGER)
//												ugt_pump_trigger_cause = MOTOR_TRIGGER_FROM_DRY_RUN;	
//											else if(temp == EVT_FORCEFULLY_UGT_PUMP_TRIGGER)	
//												ugt_pump_trigger_cause = MOTOR_TRIGGER_FROM_SWITCH;

//											else
//												ugt_pump_trigger_cause = MOTOR_TRIGGER_FROM_AUTO_TASK;

//										}
//										else{
//											if((ret_val = chk_automatic_task_status(UGT_tank.tank_config_ptr->tank_num, !(status.ugt_pump_ptr->pump_state))) == 0){
//												trigger_motor(UGT_tank.tank_config_ptr->tank_num);
//												last_pump_triggered_time_UGT = RTC_TimeStructure;
//												last_pump_triggered_date_UGT = date_obj;
//												UGT_triggered = 1;
//										//		UGT_pump_state = status.ugt_pump_ptr->pump_state;
//												event_info |= (1 << UGT_PUMP_TRIGGER); 
//												ugt_pump_trigger_cause = MOTOR_TRIGGER_FROM_SWITCH;							 	
//							 				}
//											else{
//												system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task							
//											}
//									//	}
//									}
//								//	else;
//										// due to exception schedule 
//								}
//								else
//									ugt_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
////							}
////							else{
////								ugt_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
////							}
//						}
//				   	break;
					case EVT_SCHEDULE_START_OHT_TRIGGER:
				   		#ifdef SCHEDULE_ENABLE
						if(oht_schd.schd_running_flag > 0){	   // schedule start
							if((day_time_in_minutes_glbl >= oht_schd.curr_schd.schd_start.schedule_mins) && (day_time_in_minutes_glbl <= oht_schd.curr_schd.schd_end.schedule_mins) ) {
								// Motor to be turned ON. Check schedule type.
								if (!oht_schd.curr_schd.schedule_type) {
									// Schedule is Normal Type
									if(status.oht_pump_ptr->pump_state == 0){
									//	if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
											if(((oht_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
												if((ret_val = chk_automatic_task_status(OHT_tank[0].tank_config_ptr->tank_num, ON)) == 0){
													if(oht_schd.schd_running_flag == 2){
													//	if(READ_OHT_L3 == 0 && READ_OHT_L4 == 0 && READ_OHT_L2 == 1){
														
														if(OHT_tank[0].real_sensor_ptr->highest_set_sensor <= 2){
															actuate_motor(OHT, 1);	
														}															
													}
													else
														actuate_motor(OHT, 1);
													last_pump_triggered_time_OHT = RTC_TimeStructure;
													last_pump_triggered_date_OHT = date_obj;
													OHT_triggered = 1;
												//	OHT_pump_state = status.oht_pump_ptr->pump_state;
													event_info |= (1 << OHT_PUMP_TRIGGER); 
													oht_pump_trigger_cause = 1 << MOTOR_TRIGGER_FROM_SCHEDULE;
													trigger_state.pump_cause = SYSTEM_TRIGGER;
												}
//												else{
//													system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task
//												}
											}
											else
												oht_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
									//	}
//										else{
//											oht_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
//										}
									
									}
								}
								else {
									// Schedule is Exception Type
									if(status.oht_pump_ptr->pump_state == 1){
										actuate_motor(OHT, 0);
										last_pump_triggered_time_OHT = RTC_TimeStructure;
										last_pump_triggered_date_OHT = date_obj;
										OHT_triggered = 1;
									//	OHT_pump_state = status.oht_pump_ptr->pump_state;
										oht_pump_trigger_cause = 1 << MOTOR_TRIGGER_FROM_SCHEDULE;							
									}
								}
							}
						}
						else{
							if(status.oht_pump_ptr->pump_state == 1){
							//	if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
									if(((oht_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
										if((ret_val = chk_automatic_task_status(OHT_tank[0].tank_config_ptr->tank_num, OFF)) == 0){
											actuate_motor(OHT, 0);
											last_pump_triggered_time_OHT = RTC_TimeStructure;
											last_pump_triggered_date_OHT = date_obj;
											OHT_triggered = 1;
									//		OHT_pump_state = status.oht_pump_ptr->pump_state;
											event_info |= (1 << OHT_PUMP_TRIGGER); 
											oht_pump_trigger_cause = 1 << MOTOR_TRIGGER_FROM_SCHEDULE;
											trigger_state.pump_cause = SYSTEM_TRIGGER;
										}
//										else
//											system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task																
			
									}
									else
										oht_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
//								}
//								else{
//									oht_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
//								}
						
							}
							configure_alarm (&oht_schd, oht_schedule_window);
						}
						#endif
					break;
					
//					case EVT_SCHEDULE_END_OHT_TRIGGER:
//						#ifdef SCHEDULE_ENABLE
//						if(status.oht_pump_ptr->pump_state == 1){
//							if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
//								if(((oht_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
//									if((ret_val = chk_automatic_task_status(OHT_tank[0].tank_config_ptr->tank_num, OFF)) == 0){
//										actuate_motor(OHT, 0);
//										event_info |= (1 << OHT_PUMP_TRIGGER); 
//										oht_pump_trigger_cause = MOTOR_TRIGGER_FROM_SCHEDULE;
//									}
//									else
//										system_notification_info |= (1 << (ret_val + 8));  // pump cant run due to automated task																
//		
//								}
//								else
//									oht_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
//							}
//							else{
//								oht_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
//							}
//						
//						}
//						configure_alarm (&oht_schd, oht_schedule_window);
//						#endif
//					break;
		
//					case EVT_SCHEDULE_START_UGT_TRIGGER:				//atul
//						#ifdef SCHEDULE_ENABLE
//						if(ugt_schd.schd_running_flag == 1){	   // schedule start
//							if(wms_sys_config.total_ugt > 0 && wms_sys_config.ugt_pump_select == 1){
//								if((day_time_in_minutes_glbl >= ugt_schd.curr_schd.schd_start.schedule_mins) && day_time_in_minutes_glbl <= ugt_schd.curr_schd.schd_end.schedule_mins) {
//									// Motor to be turned ON. Check schedule type.
//									if (!ugt_schd.curr_schd.schedule_type) {   // Schedule is Normal Type
//										if(status.ugt_pump_ptr->pump_state == 0){
//										//	if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
//												if(((ugt_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
//													if((ret_val = chk_automatic_task_status(UGT_tank.tank_config_ptr->tank_num, ON)) == 0){
//														actuate_motor(UGT, 1);
//														last_pump_triggered_time_UGT = RTC_TimeStructure;
//														last_pump_triggered_date_UGT = date_obj;
//														UGT_triggered = 1;
//											//			UGT_pump_state = status.ugt_pump_ptr->pump_state;
//														event_info |= (1 << UGT_PUMP_TRIGGER); 
//														ugt_pump_trigger_cause = MOTOR_TRIGGER_FROM_SCHEDULE;
//													}
////													else{
////														system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task							
////													}
//												}
//												else
//													ugt_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
////											}
////											else{
////												ugt_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
////											}
//										}
//									}				
//									else {
//										if(status.ugt_pump_ptr->pump_state == 1){
//											actuate_motor(UGT, 0);
//											last_pump_triggered_time_UGT = RTC_TimeStructure;
//											last_pump_triggered_date_UGT = date_obj;
//											UGT_triggered = 1;
//									//		UGT_pump_state = status.ugt_pump_ptr->pump_state;
//											ugt_pump_trigger_cause = MOTOR_TRIGGER_FROM_SCHEDULE;							
//										}
//									}
//								}
//							}
//						}
//						else{
//							if(wms_sys_config.total_ugt > 0 && wms_sys_config.ugt_pump_select == 1){
//								if(status.ugt_pump_ptr->pump_state == 1){
//								//	if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
//										if(((ugt_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
//											if((ret_val = chk_automatic_task_status(UGT_tank.tank_config_ptr->tank_num, OFF)) == 0){
//												actuate_motor(UGT, 0);
//												last_pump_triggered_time_UGT = RTC_TimeStructure;
//												last_pump_triggered_date_UGT = date_obj;
//												UGT_triggered = 1;
//									//			UGT_pump_state = status.ugt_pump_ptr->pump_state;
//												event_info |= (1 << UGT_PUMP_TRIGGER); 
//												ugt_pump_trigger_cause = MOTOR_TRIGGER_FROM_SCHEDULE;
//											}
////											else
////												system_notification_info |= (1 << (ret_val + 7));  // pump cant run due to automated task																
//										}
//										else
//											ugt_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
////									}
////									else{
////										ugt_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
////									}
//								}
//								configure_alarm (&ugt_schd, ugt_schedule_window);
//							}
//						}
//					#endif
//					break;
//					case EVT_SCHEDULE_END_UGT_TRIGGER:
//						#ifdef SCHEDULE_ENABLE
//							if(status.oht_pump_ptr->pump_state == 1){
//								if(((system_notification_info >> SYSTEM_ON_MAINS) & 0x01) == 0x01){
//									if(((ugt_notification_info >> PUMP_WIRE_CONNECT) & 0x01) == 0x01){
//										if((ret_val = chk_automatic_task_status(UGT_tank.tank_config_ptr->tank_num, OFF)) == 0){
//											actuate_motor(UGT, 0);
//											event_info |= (1 << UGT_PUMP_TRIGGER); 
//											ugt_pump_trigger_cause = MOTOR_TRIGGER_FROM_SCHEDULE;
//										}
//										else
//											system_notification_info |= (1 << (ret_val + 8));  // pump cant run due to automated task																
//									}
//									else
//										ugt_notification_info |= (1 << PUMP_CANT_START_WIRE_DISCONNECT);
//								}
//								else{
//									ugt_notification_info |= (1 << PUMP_CANT_START_AC_MAINS_OFF);
//								}
//							}
//							configure_alarm (&ugt_schd, ugt_schedule_window);
//						#endif
//					break;
		
					
//					case EVT_POWER_FAILURE_PUMP_OFF:	
//						if(READ_POWER_PIN == 1){
//							if(status.oht_pump_ptr->pump_state == 1 && oht_pump_wire_disconnect == 1){								  /* TURN OHT Pump off */
//								actuate_motor(OHT, 0);
//								oht_pump_trigger_cause = MOTOR_TRIGGER_POWER_FAILURE;
//								event_info |= (1 << OHT_PUMP_TRIGGER); 
//								trigger_state.pump_cause = SYSTEM_TRIGGER;
//								oht_pump_wire_disconnect = 0;
//							}
//							if(wms_sys_config.ugt_pump_select == 1 && ugt_pump_wire_disconnect == 1){						   /* Turn UGT pump off */
//								if(status.ugt_pump_ptr->pump_state == 1){
//									actuate_motor(UGT, 0);
//									ugt_pump_trigger_cause = MOTOR_TRIGGER_POWER_FAILURE;
//									event_info |= (1 << UGT_PUMP_TRIGGER); 
//									ugt_pump_wire_disconnect = 0;
//								}
//							}	
//
//						}
//						else{
//							if(status.oht_pump_ptr->pump_state == 1){								  /* TURN OHT Pump off */
//								actuate_motor(OHT, 0);
//								oht_pump_trigger_cause = MOTOR_TRIGGER_POWER_FAILURE;
//								event_info |= (1 << OHT_PUMP_TRIGGER); 
//								trigger_state.pump_cause = SYSTEM_TRIGGER;
//							}
//							if(wms_sys_config.ugt_pump_select == 1){						   /* Turn UGT pump off */
//								if(status.ugt_pump_ptr->pump_state == 1){
//									actuate_motor(UGT, 0);
//									ugt_pump_trigger_cause = MOTOR_TRIGGER_POWER_FAILURE;
//									event_info |= (1 << UGT_PUMP_TRIGGER); 
//								}
//							}
//						}
//					break;
				}
//			}
		}
			

////			for(count = 0 ; count < 3 ; count++){
////				if(send_data_json_server(json_send_arr)){
////					step_check_flag = 7;
////					break;
////				}
////				else{
////					os_dly_wait(10);	
////				}
////			}								
//		}
//		if(prev_ugt_status != status.ugt_pump_ptr->pump_state){
//			if(wms_sys_config.total_ugt > 0 && wms_sys_config.ugt_pump_select == 1){
//				create_GUI_tx_packet(GET_PUMP_STATUS,&UGT_tank, 0);
//				send_data_json_server(json_send_arr);
////				for(count = 0 ; count < 3 ; count++){
////					if(send_data_json_server(json_send_arr)){
////						break;
////					}
////					else{
////						os_dly_wait(10);	
////					}
////				}								
//			}
//		}
//			save_pump_state();
//			reset_loc_flags();
		}
		
				// SEND DATA TO SERVER
		if(prev_oht_status != status.oht_pump_ptr->pump_state){
//			step_check_flag = 5;
//			create_GUI_tx_packet(GET_PUMP_STATUS,&OHT_tank[0], 0);
				create_pump_status_packet();
			
//			step_check_flag = 6;			
			
			length = strlen((char *)json_send_arr);
			memset(sendbuf,0x20,1000);
			strncpy((char *)sendbuf, (char *)json_send_arr, length);
//			uart_send_str(USART2, json_send_arr, length);
			usb_send_data(sendbuf, (length) );
			//send_data_json_server(json_send_arr);
		}
		pump_evt_num = 0;
		pump_monitoring = 0;
	}
}



void scan_periodic_tsk(void) {

	uint8_t temp, count,  buff[20];
//	static uint32_t last_system_notification_info;


//   // check oht notification
//	if(notification_counter_oht >= 20) {
//		
//	   // check system notification
//	 if(last_system_notification_info != system_notification_info){
//	 	for(count = 1; count < 32; count++){
//			if(((system_notification_info >> count & 0x01) == 0x01) && (last_system_notification_info >> count & 0x01) == 0x0){
//				//save_notification(&OHT_tank[0], count, &buff[0]);
//				create_notification_status_packet(&buff[1]);
//				usb_send_data(json_send_arr, strlen(json_send_arr));
////				for(count = 0 ; count < 3 ; count++){
////					if(send_data_json_server(json_send_arr)){
////						break;
////					}
////					else{
////						os_dly_wait(50);	
////					}
////				}								
//			}
//		}
//	//	system_notification_info = system_notification_info & 0xfc007f; // other notification states should be require
//		system_notification_info = system_notification_info & 0x7fc007f; // other notification states should be require
//		last_system_notification_info = system_notification_info;		
//	 }
//	notification_counter_oht = 0;
// }

 

	if(adc_monitoring) {

		for(temp = 0; temp < 4; temp++){
			if((((adc_evt_num >> temp) & 0x01) == 0x01)){
				switch(temp + 1){
//					case EVT_ONE_SEC_EVNT:  		/*  */
//						scan_periodic_tsk();				
//						tmr_1 = os_tmr_create(TWO_SEC*200, 1);	
//					break;
					
					case EVT_OHT_FEEDBACK_CHNL:
					/* send packet */
					//	if(READ_POWER_PIN == 1 && power_pin_current_status == 1){
							error_flag_num = (oht_feedback_volt > 0) ? PUMP_WIRE_CONNECT:PUMP_WIRE_DISCONNECT;
							if(oht_feedback_volt > 0){
								oht_notification_info |= (1 << PUMP_WIRE_CONNECT);
								oht_notification_info &= ~(1 << PUMP_WIRE_DISCONNECT);						 
							}
							else {
								oht_pump_wire_disconnect = 1;
							//	os_evt_set (POWER_FAILURE_PUMP_OFF, tid_pump_evt);
								oht_notification_info &= ~(1 << PUMP_WIRE_CONNECT);						 
								oht_notification_info |= (1 << PUMP_WIRE_DISCONNECT); 
							}
					//	}					
						
					break;
		
					case EVT_UGT_FEEDBACK_CHNL:
					/* send packet */
					//	if(READ_POWER_PIN == 1 && power_pin_current_status == 1){
							error_flag_num = (ugt_feedback_volt > 0) ? PUMP_WIRE_CONNECT:PUMP_WIRE_DISCONNECT;
							if(ugt_feedback_volt > 0){
								ugt_notification_info |= (1 << PUMP_WIRE_CONNECT);
								ugt_notification_info &= ~(1 << PUMP_WIRE_DISCONNECT);						 
							}
							else {
								ugt_pump_wire_disconnect = 1;
							//	os_evt_set (POWER_FAILURE_PUMP_OFF, tid_pump_evt);
								ugt_notification_info &= ~(1 << PUMP_WIRE_CONNECT);						 
								ugt_notification_info |= (1 << PUMP_WIRE_DISCONNECT); 
							}
					//	}
					break;
						
				}
			}
		}
	 }
	 adc_monitoring = 0;
}



void notification_info_oht(void) {
	uint8_t temp, count,  buff[20] ;
	uint8_t sendbuf_notification[1000];
	uint16_t len;
	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		  // read the RTC time structure
	RTC_GetDate(RTC_Format_BIN, &date_obj);					  // read the RTC date structure
	
	buff[0] = 1;
	buff[2] = 0;
	buff[3] = 0;
	buff[4] = date_obj.RTC_Year;
	buff[5] = date_obj.RTC_Month;
	buff[6] = date_obj.RTC_Date;
	buff[7] = RTC_TimeStructure.RTC_Hours;
	buff[8] = RTC_TimeStructure.RTC_Minutes;
	buff[9] = RTC_TimeStructure.RTC_Seconds;
	
	
	
	for(temp = 0; temp < 32 ; temp++){             //notification number
		if( ((oht_notification_info >> temp) & (0x01) ) == 1)
			buff[1] =  temp;
	}
	
	for(temp = 0; temp < 32 ; temp++){             //notification field
		if( ((oht_notification_info >> temp) & (0x01) ) == 1)
			buff[2] =  temp;
	}
	
	oht_notification_info &= ~(1 << 15);              //Clear previous automatic task
	
//	static uint32_t last_system_notification_info;
	
	   // check oht notification
//	if(notification_counter_oht >= 20) {
		
	   // check system notification
//	 if(last_system_notification_info != system_notification_info){
//	 	for(count = 1; count < 32; count++){
//			if(((system_notification_info >> count & 0x01) == 0x01) && (last_system_notification_info >> count & 0x01) == 0x0){
				//save_notification(&OHT_tank[0], count, &buff[0]);
	
//				create_notification_status_packet(&buff[0]);
//				usb_send_data(json_send_arr, strlen(json_send_arr));
	
				create_notification_status_packet(&buff[0]);
				len = strlen((char *)json_send_arr);
				memset(sendbuf_notification,0x20,1000);
				strncpy((char *)sendbuf_notification, (char *)json_send_arr, (len) );	 
		//		uart_send_str(USART2, json_send_arr, len);
				usb_send_data(sendbuf_notification, (len));
				
//				for(count = 0 ; count < 3 ; count++){
//					if(send_data_json_server(json_send_arr)){
//						break;
//					}
//					else{
//						os_dly_wait(50);	
//					}
//				}								
	//		}
	//	}
	//	system_notification_info = system_notification_info & 0xfc007f; // other notification states should be require
//		system_notification_info = system_notification_info & 0x7fc007f; // other notification states should be require
//		last_system_notification_info = system_notification_info;		
//	 }
//	notification_counter_oht = 0;
 //}
}


/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/

