/********************************************************************************************************************************
 * File name:	 	target.c
 *
 * Attention:		Copyright 2015 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 22/12/2015 by Sonam
 *
 * Description: 	This module is used to initialize all peripheral on Cool Smart.
 *******************************************************************************************************************************/


/********************************************************************************************************************************
 * Include Section
 *******************************************************************************************************************************/
#include "stm32f0xx.h"
#include "uart.h"
#include "timer.h"
#include "adc.h"
#include "i2c.h"
#include "eeprom.h"
#include "led.h"
#include "pin_config.h"
#include "watchdog.h"
#include "stm32f0xx_rcc.h"
#include "button_input.h"
#include "std_periph_headers.h"
#include "profile.h"
#include "rtc.h"
#include "water_management.h"
#include "buzzer.h"
#include "memory_map.h"
#include "json_client.h"
#include "client_registration.h"
/********************************************************************************************************************************
* Defines section
 *******************************************************************************************************************************/

/********************************************************************************************************************************
* Global declarations section
 *******************************************************************************************************************************/
extern button device_mode;
extern device_profile profile;
uint8_t di_return = 0;
extern uint8_t  OHT_triggered, UGT_triggered;

extern struct buzzer_switch_behavior buzzer_switch_info;
extern struct buzzer_interval 	buzzer_interval_info;

extern volatile uint8_t wifi_initialised;

extern uint32_t adc_channel_type;

extern uint16_t day_time_in_minutes_glbl;
extern RTC_DateTypeDef date_obj, RTC_DateStruct;
extern RTC_TimeTypeDef RTC_TimeStructure;

extern struct wms_sys_info wms_sys_config;
#ifdef MULTI_TANK_ENABLE
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
	extern struct tank_status status;	
#else
	extern struct tank_info OHT_tank[TOTAL_OHT_TANK], UGT_tank;	
#endif
extern struct loc_tank_trigger_flags trigger_state;


extern 	uint16_t buzzer_activation_counter;//atul
extern 	uint8_t buzzer_activation_flag;

uint8_t time[] = {0,17,11,4,1};

/********************************************************************************************************************************
 * Function name: 	void init_target(void)
 *
 * Returns: 		None
 *
 * Arguments: 		None
 *
 * Created by: 		
 *
 * Date created: 	
 *
 * Description: 	Initializes all the modules present in Cool Smart.
 *
 * Notes:
 *******************************************************************************************************************************/
void Init_Target(void){
 uint8_t profile_data[100] , read_buff[100], count, indx, loc, buff[20], temp_arr[50], perform_default_wr;
  uint32_t loc_var, level = 0 ;

	/**********************************************************/
	/*			 Initialize OAMP & Device LEDs 				  */
	/**********************************************************/
	
	
 
	led_Init();
 
	


//  TURN_OHT_GREEN;	  //atul
//  TURN_OHT_RED;			//atul
//	TURN_OHT_ORANGE;	//atul
//  TURN_OHT_OFF;			//atul

//	TURN_UGT_GREEN;			//atul
//	TURN_UGT_RED;				//atul
//	TURN_UGT_ORANGE;		//atul	
//	TURN_UGT_OFF;				//atul

//	TURN_OAMP_ON
//	TURN_OAMP_OFF


//	SET_BUZZER;			//Atul
//	CLR_BUZZER;			//Atul


	
	/**********************************************************/
	/*			 Initialize Relay at PA0 && PA1 				  */
	/**********************************************************/

	 RELAY_OHT_Init();
//	 RELAY_UGT_Init();

//	 RELAY_OHT_ENABLE;		//atul
//	 RELAY_UGT_ENABLE;



	/**********************************************************/
	/*	 Initialize Digital Inputs PB0-PB-15 && PC5-PC7 	  */
	/**********************************************************/


	init_Digitalinputs ();
//	di_return = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_11);
//	di_return = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_12);
//	di_return = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_13);
//	di_return = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_14);
//	di_return = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_15);
//	di_return = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8);
//	di_return = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_9);
//	di_return = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_15);
//	di_return = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_3);
//	di_return = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_4);
//	di_return = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1);

	 



	/**********************************************************/
	/*			Initialize Button on Ext Interrupt 			  */
	/**********************************************************/


//	 EXTILine8_Config();	// Configure External Input ResetButton as Input
		 EXTILine2_Config();	// Configure External Input PUMP1Button as Input
//	 EXTILine10_Config();	// Configure External Input PUMP2Button as Input

	 
	 
	 
	 
// iic_test(); //i2C as GPIO

// GPIO_SetBits(GPIOB, GPIO_Pin_8);
// GPIO_SetBits(GPIOB, GPIO_Pin_9);

// GPIO_ResetBits(GPIOB, GPIO_Pin_8);
// GPIO_ResetBits(GPIOB, GPIO_Pin_9);

	/**********************************************************/
	/*					   Initialize EEPROM 				  */
	/**********************************************************/
	 i2c_init ();
	 EEPROM_WP_init();
	 
//	EEPROM_WRITE_ACCESS_ENABLE;
//	EEPROM_WRITE_ACCESS_DISABLE;
	 
//Read Profile data from EEPROM
//	eeprom_data_read_write(0x00, WRITE_OP,profile_data, 40);   //atul
//	eeprom_data_read_write(0x00, READ_OP, read_buff, 40);			//atul

//	profile_write(1, &profile);	  
//	profile_trigger(1);


	/*					   Initialize Timers 				  */
	/**********************************************************/

	timer_init (TIM3, TIME_MILLI_SEC, 1000);	//Sensor control
  TIM_Cmd (TIM3, ENABLE);

	timer_init(TIM16,TIME_MILLI_SEC, 10);     //Led Display control
  TIM_Cmd (TIM16, ENABLE);

	timer_init(TIM14,TIME_MILLI_SEC, 1000);    //Buzzer Control
	TIM_Cmd (TIM14, ENABLE);
	
//	timer_init(TIM17,TIME_MICRO_SEC, 15);    //Buzzer Control   //atul
//	TIM_Cmd (TIM17, ENABLE);
	


  /**********************************************************/
	/*					   Initialize UARTS 				  */
	/**********************************************************/

	uart_init (USART2, 115200);	
//	while(1) {
//	uart_send (USART2, 'H');				//atul
//	uart_send_str(USART2, "atul",4);
//	}

	/**********************************************************/
	/*			Initialize ADC for Battery & Version 		  */
	/**********************************************************/

	ADC_1_Config();
 // wifi_enable();
	
	/**********************************************************/
	/*			Initialize RTC                      		  */
	/**********************************************************/

	RTC_initialize();
  //write_RTC(time, 2017);

	adc_channel_type = ADC_Channel_1;
	select_feedback_chnl();

	/**********************************************************/
	/*				set alarm					  */
	/**********************************************************/	
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);		  				// read the RTC time structure
	RTC_GetDate(RTC_Format_BIN, &date_obj);					  				// read the RTC date structure
	day_time_in_minutes_glbl = (RTC_TimeStructure.RTC_Hours*60) + RTC_TimeStructure.RTC_Minutes;	 // calculate time in minutes
//  current_day_num = get_day_num(date_obj.RTC_Date, date_obj.RTC_Month, date_obj.RTC_Year);
//	prev_day_num = current_day_num;
//	loc_var = ((day_time_in_minutes_glbl / 60) + 1)* 60;
//	set_alarm_in_rtc(loc_var);



  /**********************************************************/
	/*			Integration to LowCost                    		  */
	/**********************************************************/


  /**********************************************************/
	/*		   Initialize global structure			  		  */
	/**********************************************************/

		initialize_global_structures();


	/**********************************************************/
	/*		   Default client registration			  		  */
	/**********************************************************/

	
  eeprom_data_read_write(APPL_DEFAULT_WR_CHECK_ADDR, READ_OP, &perform_default_wr, 1);
	if(perform_default_wr != 'Y')
	 {
		loc	= 'N';	   	
		eeprom_data_read_write(SYSTEM_INFO_ADDR, WRITE_OP, &loc, 1);    /* board status */
		temp_arr[0] = '\0';
		for(count = 0; count < TOTAL_REG_KEY; count++){
			eeprom_data_read_write(DEVICE_REG_START_ADDR + count*DEVICE_REG_LEN, WRITE_OP, &temp_arr[0], 1);
		}
		clear_all_registered_clients();
		write_one_time_settings();
		write_default_configuration();
		save_default_schedules();
	//clear_consumption_logs();

		perform_default_wr = 'Y';
		eeprom_data_read_write(APPL_DEFAULT_WR_CHECK_ADDR, WRITE_OP, &perform_default_wr, 1);
	 }


	initialize_global_structures();
	restore_configured_settings();   
	read_pin_status();
	for(count = 0; count < wms_sys_config.total_oht; count++){								/* read real sensor */
		get_current_level(&OHT_tank[count]);	
		level += OHT_tank[count].current_level;
	}
	status.oht_current_level = (level/wms_sys_config.total_oht);
	if(wms_sys_config.total_ugt > 0){
		get_current_level(&UGT_tank);
		status.ugt_current_level =  UGT_tank.current_level;
	}
	trigger_state.prev_ugt_highest_set_sensor = UGT_tank.real_sensor_ptr->highest_set_sensor;		   // check
	for(indx = 0; indx < TOTAL_OHT_TANK; indx++){
		trigger_state.prev_oht_highest_set_sensor[indx] = OHT_tank[indx].real_sensor_ptr->highest_set_sensor;
	}
	calculate_average_level();
	#ifdef VIRTUAL_SENSOR
		calibrate_oht_virtual_system(0);
		if(wms_sys_config.total_tank > 1)
			calibrate_ugt_virtual_system(0);
	#endif
	automated_task_process();
	read_schedules (OHT_tank[0].tank_config_ptr->tank_num);
	wifi_initialised = 1;
	
}
