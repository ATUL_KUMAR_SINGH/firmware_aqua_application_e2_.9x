/************************************************************************//**
* @file			button_input.c
*
* @brief		This module contains the Button Functionality in Cool Master Application.
*
* @attention	Copyright 2015 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 22/12/15 \n by \Sonam
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			 
****************************************************************************/

/*
**===========================================================================
**		Include section
**===========================================================================
*/
#include "stm32f0xx.h"
#include "std_periph_headers.h"
#include "button_input.h"
#include "led.h"

button device_mode;

extern volatile uint8_t stop_mode_exit;

extern uint8_t profile_mode, shutdown_mode, button_press_flag_count, twice_press_state;
void delay_temp(void);
volatile uint8_t intr_cnt = 0,button_irq_state = 0, button_press_flag = 0;

uint8_t oht_key_debounce_flag, oht_key_debounce_counter, oht_switch_status_curr, oht_switch_status_prev, ugt_key_debounce_flag, ugt_key_debounce_counter, ugt_switch_status_curr, ugt_switch_status_prev, oamp_key_debounce_flag, oamp_key_debounce_counter, oamp_switch_status_curr, oamp_switch_status_prev;
extern uint8_t ugt_switch_low_flag,ugt_switch_low_count;





/**
  * @brief  This function handles External line 0 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI2_3_IRQHandler(void) {

  if(EXTI_GetITStatus(EXTI_Line2) != RESET) {
	oht_key_debounce_flag = 1;
	oht_key_debounce_counter = 0;

	/* Clear the EXTI line 2 pending bit */
   EXTI_ClearITPendingBit(EXTI_Line2);
  }


}

/************************************************************************//**
*					void EXTILine8_Config(void)
*
* @brief			This routine is used to initialize External Line interrupt for ResetButton.
*
* @param *field		void
* @param nob		void
*
* @returns			CVoid
*
* @exception		None.
*
* @author			
* @date				
* @note				None.                                       
****************************************************************************/
void EXTILine8_Config(void)
{
  EXTI_InitTypeDef   EXTI_InitStructure;
  GPIO_InitTypeDef   GPIO_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

  /* Enable GPIOC clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  
  /* Configure PC9 pin as input floating */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_8;
  GPIO_Init(GPIOC, &GPIO_InitStructure);


  /* Connect EXTI Line8 to PC8 pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource8);

  /* Configure EXTI Line8 */
  EXTI_InitStructure.EXTI_Line = EXTI_Line8;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger =   EXTI_Trigger_Falling;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI Line8 Interrupt to the First priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0x01;
  //NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);			  
}




/************************************************************************//**
*					void EXTILine9_Config(void)
*
* @brief			This routine is used to initialize External Line interrupt for PUMPButton1.
*
* @param *field		void
* @param nob		void
*
* @returns			CVoid
*
* @exception		None.
*
* @author			
* @date				
* @note				None.                                       
****************************************************************************/
void EXTILine2_Config(void)
{
  EXTI_InitTypeDef   EXTI_InitStructure;
  GPIO_InitTypeDef   GPIO_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

  /* Enable GPIOB clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  
  /* Configure PB2 pin as input floating */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_2;
  GPIO_Init(GPIOB, &GPIO_InitStructure);


  /* Connect EXTI Line2 to PB2 pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource2);

  /* Configure EXTI Line2 */
  EXTI_InitStructure.EXTI_Line = EXTI_Line2;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI Line2 Interrupt to the Second priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI2_3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0x02;
 // NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);			  
}




/************************************************************************//**
*					void EXTILine10_Config(void)
*
* @brief			This routine is used to initialize External Line interrupt for PUMP Button2.
*
* @param *field		void
* @param nob		void
*
* @returns			CVoid
*
* @exception		None.
*
* @author			
* @date				
* @note				None.                                       
****************************************************************************/
void EXTILine10_Config(void)
{
  EXTI_InitTypeDef   EXTI_InitStructure;
  GPIO_InitTypeDef   GPIO_InitStructure;
  NVIC_InitTypeDef   NVIC_InitStructure;

  /* Enable GPIOC clock */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

  /* Enable SYSCFG clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
  
  /* Configure PC10 pin as input floating */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_10;
  GPIO_Init(GPIOC, &GPIO_InitStructure);


  /* Connect EXTI Line10 to PC10 pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource10);

  /* Configure EXTI Line10 */
  EXTI_InitStructure.EXTI_Line = EXTI_Line10;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger =   EXTI_Trigger_Falling;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI Line10 Interrupt to the Third priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0x03;
  //NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x02;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);			  
}







void init_switch (void) {

	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);	

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_Level_3;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 ;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

}


void init_Digitalinputs (void) {

	GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef  NVIC_InitStructure;
  EXTI_InitTypeDef  EXTI_InitStructure;
	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);	
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);	

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType= GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;    
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_Level_3;

	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_15 ;  //PA1 = Vsense
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_3 |  GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;  //used for wifi reset
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	//	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_Level_1;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13  ;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	
	
 	/*********** LPM PC13 CONFIGURATION ****************************/
	  /* Connect EXTI Line1 to PC13 pin */
  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);

  /* Configure EXTI Line13 */
  EXTI_InitStructure.EXTI_Line = EXTI_Line13;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);

  /* Enable and set EXTI Line2 Interrupt to the Second priority */
  NVIC_InitStructure.NVIC_IRQChannel = EXTI4_15_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0x00;
 // NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

}


uint8_t SWITCH_PUMP1() {

uint8_t di_state = 255;
di_state = GPIO_ReadInputDataBit(SWITCH_PUMP1_PORT, SWITCH_PUMP1_PIN);
return 	di_state;
}


uint8_t SWITCH_PUMP2() {

uint8_t di_state = 255;
di_state = GPIO_ReadInputDataBit(SWITCH_PUMP2_PORT, SWITCH_PUMP2_PIN);
return 	di_state;
}


uint8_t SWITCH_RST_BZR() {

uint8_t di_state = 255;
di_state = GPIO_ReadInputDataBit(SWITCH_RST_BZR_PORT, SWITCH_RST_BZR_PIN);
return 	di_state;
}


uint8_t ReadDigitalInput(GPIO_TypeDef* DI_PORT, uint16_t DI_PIN) {

uint8_t di_state = 255;
di_state = GPIO_ReadInputDataBit(DI_PORT, DI_PIN);
return 	di_state;
}




