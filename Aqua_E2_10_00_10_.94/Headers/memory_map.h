/************************************************************************//**
* @file			"memory_map.h"
*
* @brief		Header for 
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 12/01/13 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
/*
**===========================================================================
**		Include section
**===========================================================================
*/
#ifndef MEMORY_MAP_H
#define MEMORY_MAP_H

#include "stm32f0xx.h"


/*
**===========================================================================
**		Defines section
**===========================================================================
*/

/* memory address */

#define		SYSTEM_INFO_ADDR					1			/* board is new or not */

#define		SYSTEM_MAC_ID_INFO_ADDR				2		   /* MAC id length 6 bytes */

#define		LICENSE_NUM_INFO_ADDR				8		   /* license num length 16 bytes */

#define		REG_KEY_INFO_ADDR					32

#define		DHCP_INFO_ADDR						92

#define		NW_INFO_ADDR						93		//16 BYTES

#define		ADMIN_PASSWRD_INFO_ADDR				109		// 6 BYTES

#define		SERIAL_NUM_INFO_ADDR				115		// 15 bytes

#define		BUZZER_SETTING_INFO_ADDR			130		// 3 bytes 

#define		TOTAL_EVENT_LOG_COUNT				133		// 1 byte

#define		IMMEDIATE_DATA						134			// 14 BYTE 

#define		TOTAL_NOTIFICATION_LOG_COUNT		148		// 1 BYTE

#define		WMS_NEW_FIRMWARE_PRESENT_ADDRESS		149

#define		BOOT_ERR_CHK_ADDR						150

#define		PRIMARY_IMG_STATUS						151

#define		APPLICATION_ADDR						152		// 4 bytes

#define		TOTAL_OHT_LOOK_UP_ENTRY					156		// 1 BYTE

#define		TOTAL_UGT_LOOK_UP_ENTRY					157		// 1 BYTE

#define		CURRENT_FW_ADDR							158		// 3 BYTES

#define		UPGRADING_FW_ADDR						161		// 3 BYTES

#define 	BOOT_DEFAULT_WR_CHECK_ADDR				164

#define 	APPL_DEFAULT_WR_CHECK_ADDR				165

#define		TOTAL_REG_KEY_ADDR						170		// 1 BYTE

#define   TANK_PUMP_SENSOR_SETTING_ADDR   171



#define 	DEVICE_REG_START_INFO_ADDR			200

#define  	MAX_REG_KEY											3

#define  	DEVICE_REG_LEN									57
	
#define 	DEVICE_REG_END_INFO_ADDR			DEVICE_REG_START_INFO_ADDR + MAX_REG_KEY*DEVICE_REG_LEN  + 231

#define 	RFU_START_ADDR						DEVICE_REG_END_INFO_ADDR

#define 	RFU_END_ADDR						RFU_START_ADDR + 2*TANK_PUMP_INFO_LEN

//#define		TANK_PUMP_START_INFO_ADDR			DEVICE_REG_END_INFO_ADDR
//
//#define		OHT_TANK_PUMP_START_INFO_ADDR		TANK_PUMP_START_INFO_ADDR
//
//#define		OHT_TANK_PUMP_END_INFO_ADDR			OHT_TANK_PUMP_START_INFO_ADDR + TANK_PUMP_INFO_LEN
//
//#define		UGT_TANK_PUMP_START_INFO_ADDR		OHT_TANK_PUMP_END_INFO_ADDR
//
//#define		UGT_TANK_PUMP_END_INFO_ADDR			UGT_TANK_PUMP_START_INFO_ADDR + TANK_PUMP_INFO_LEN

#define		WATER_USAGE_INFO_START_ADDR				RFU_END_ADDR

#define		WATER_USAGE_INFO_END_ADDR				WATER_USAGE_INFO_START_ADDR	 + WATER_USAGE_LEN

#define		TANK_PUMP_EVENT_START_ADDR			  	WATER_USAGE_INFO_END_ADDR

#define		TANK_PUMP_EVENT_END_ADDR				TANK_PUMP_EVENT_START_ADDR + TANK_PUMP_RECORD_LEN*MAX_EVENT

#define	   NOTIFICATION_START_ADDR					TANK_PUMP_EVENT_END_ADDR

#define	   NOTIFICATION_END_ADDR					NOTIFICATION_START_ADDR + NOTIFICATION_LEN*MAX_NOTIFICATION


#define	 TOTAL_OHT_TANK_CONSUMPTION_ADDR		NOTIFICATION_END_ADDR

#define	 OHT_TANK_CONSUMPTION_START_ADDR	 	TOTAL_OHT_TANK_CONSUMPTION_ADDR + 1

#define	 ONE_HOUR_CONSUMPTION_LEN				4

#define	 ONE_DAY_CONSUMPTION_LEN				103     /* year number - 1 byte, day number - 2 byte, 24 hours * 4 = 96 bytes and last 4 bytes holds the average consumption of day */

#define	 NO_OF_DAYS								1

#define	 OHT_TANK_CONSUMPTION_END_ADDR			OHT_TANK_CONSUMPTION_START_ADDR + NO_OF_DAYS*ONE_DAY_CONSUMPTION_LEN

#define	 TOTAL_UGT_TANK_CONSUMPTION_ADDR		OHT_TANK_CONSUMPTION_END_ADDR

#define	 UGT_TANK_CONSUMPTION_START_ADDR	 	TOTAL_UGT_TANK_CONSUMPTION_ADDR + 1
//
#define	 ONE_HOUR_CONSUMPTION_LEN				4
//
#define	 ONE_DAY_CONSUMPTION_LEN				103     /* year number - 1 byte, day number - 2 byte, 24 hours * 4 = 96 bytes and last 4 bytes holds the average consumption of day */
//
#define	 NO_OF_DAYS								30
//
#define	 UGT_TANK_CONSUMPTION_END_ADDR			UGT_TANK_CONSUMPTION_START_ADDR + NO_OF_DAYS*ONE_DAY_CONSUMPTION_LEN


//#ifdef FW_VER_GREATER_THAN_3_7_25
//	
//#else
	
//#endif

#define		ALARM_INFO_START_ADDR		UGT_TANK_CONSUMPTION_END_ADDR

#define		OHT_ALARMS_OFFSET					ALARM_INFO_START_ADDR

#define		OHT_ALARM_ADDR(x)					(OHT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * x))

#define		UGT_ALARMS_OFFSET					(OHT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * MAX_SCHEDULES))

#define		UGT_ALARM_ADDR(x)					(UGT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * x))

#define		BUZZER_ALARMS_OFFSET				(UGT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * MAX_SCHEDULES))

#define		BUZZER_ALARM_ADDR(x)				(BUZZER_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * x))

#define		ALARM_INFO_END_ADDR					(BUZZER_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * MAX_SCHEDULES))

#define		OHT_CONSUMPTION_LOOK_UP_START_ADDR	ALARM_INFO_END_ADDR

#define		LOOK_UP_LEN							4

#define		OHT_CONSUMPTION_LOOK_UP_END_ADDR	OHT_CONSUMPTION_LOOK_UP_START_ADDR + LOOK_UP_LEN* NO_OF_DAYS

#define		LOOK_UP_LEN							4

#define		UGT_CONSUMPTION_LOOK_UP_START_ADDR	  OHT_CONSUMPTION_LOOK_UP_END_ADDR

#define		UGT_CONSUMPTION_LOOK_UP_END_ADDR	  UGT_CONSUMPTION_LOOK_UP_START_ADDR + LOOK_UP_LEN* NO_OF_DAYS
	

#define		OHT_SETTING_START_ADDR			  	UGT_CONSUMPTION_LOOK_UP_END_ADDR

#define		TANK_SETTING_LEN					35

#define		OHT_SETTING_END_ADDR			 	OHT_SETTING_START_ADDR + TOTAL_OHT_TANK*TANK_SETTING_LEN 

#define		UGT_SETTING_START_ADDR				OHT_SETTING_END_ADDR

#define		UGT_SETTING_END_ADDR				UGT_SETTING_START_ADDR + TANK_SETTING_LEN

#define		NEW_ALARM_INFO_START_ADDR				UGT_TANK_CONSUMPTION_END_ADDR

#define		NEW_OHT_ALARMS_OFFSET					NEW_ALARM_INFO_START_ADDR

#define		NEW_OHT_ALARM_ADDR(x)					(NEW_OHT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * x))

#define		NEW_UGT_ALARMS_OFFSET					(NEW_OHT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * MAX_SCHEDULES))

#define		NEW_UGT_ALARM_ADDR(x)					(NEW_UGT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * x))

#define		NEW_BUZZER_ALARMS_OFFSET				(NEW_UGT_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * MAX_SCHEDULES))

#define		NEW_BUZZER_ALARM_ADDR(x)				(NEW_BUZZER_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * x))

#define		NEW_ALARM_INFO_END_ADDR					(NEW_BUZZER_ALARMS_OFFSET + (sizeof (schedule_mem_struct) * MAX_SCHEDULES))

#define		SERVER_LICENSE_ADDR						NEW_ALARM_INFO_END_ADDR  // totsl bytes used 25

#define 	BUYERS_INFO_START_ADDR				SERVER_LICENSE_ADDR + 26

#define		BUYERS_NAME_ADDR							BUYERS_INFO_START_ADDR

#define		BUYERS_CONTACT_NO_ADDR				BUYERS_NAME_ADDR + BUYERS_NAME_LEN

#define		BUYERS_E_MAIL_ID_ADDR					BUYERS_CONTACT_NO_ADDR + BUYERS_CONTACT_NO_LEN

#define   BUYERS_INFO_END_ADDR					(BUYERS_INFO_START_ADDR + BUYERS_NAME_LEN + BUYERS_CONTACT_NO_LEN + BUYERS_E_MAIL_ID_LEN)

//#define 	CURRENT_END_ADDR							BUYERS_INFO_END_ADDR



#define MAX_DIFF_TYPE_USERS							1
#define BYTES_ALLOCATED_FOR_EACH_USER				23   

#define DIFF_USER_TYPES_PWD_INFO_START_ADDR			 BUYERS_INFO_END_ADDR
#define DIFF_USER_TYPE_START_ADDR(x)				    (DIFF_USER_TYPES_PWD_INFO_START_ADDR + (BYTES_ALLOCATED_FOR_EACH_USER * (x - 1)))   // "x" can be (1 - 16)	1 = ADMIN_USER, 2 = STD_USER, 3 = GUEST_USER, 4 = TRCH_USER					
#define DIFF_USER_TYPES_PWD_INFO_END_ADDR			  (DIFF_USER_TYPES_PWD_INFO_START_ADDR + (BYTES_ALLOCATED_FOR_EACH_USER * MAX_DIFF_TYPE_USERS))

#define GMT_STANDARD_ADDRESS                (DIFF_USER_TYPES_PWD_INFO_END_ADDR)      //30 byte space
	
  /* MACROS */

#define		SYSTEM_MAC_ID_LEN					6

#define		SERIAL_NUM_LEN						12

#define		LICENSE_NUM_LEN						24

#define		NAME_LEN							11

#define		PHONE_LEN							11

#define		REGISTRATION_KEY_LEN				4	

#define		TOTAL_TANKS							2

#define		TANK_PUMP_INFO_LEN					42

#define		TOTAL_AUTOMATED_TASK				7

#define		WATER_USAGE_LEN						20

#define		TANK_PUMP_RECORD_LEN				10		 	/* type - pump or tank (1 byte), state - 1 byte,  */

#define		MAX_EVENT							1

#define		NOTIFICATION_LEN					11		 	

#define		MAX_NOTIFICATION					30

#define		VERSION_NUM_LEN						3



#endif
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
