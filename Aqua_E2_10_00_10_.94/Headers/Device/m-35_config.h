   /************************************************************************//**
* @file			m-35_config.h
*
* @brief		Header for m-35_config.c
*
* @attention	Copyright 2016 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 03/10/16 \n by \em Harmeen
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
****************************************************************************/
#ifndef __M_35_CONFIG_H 
#define __M_35_CONFIG_H 

/*
**===========================================================================
**		Include section
**===========================================================================
*/

/*
**===========================================================================
**		Defines section
**===========================================================================
*/
#define CONFIG_COMMAND	 22

/*
**===========================================================================
**		Function Declarations
**===========================================================================
*/


#endif // 
/******************************************************************************
**                            End Of File
******************************************************************************/