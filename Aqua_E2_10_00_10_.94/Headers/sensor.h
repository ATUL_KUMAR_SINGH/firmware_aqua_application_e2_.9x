/************************************************************************//**
* @file			sensor.h
*
* @brief		Header for sensor.c
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 20/04/14 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
/*
**===========================================================================
**		Include section
**===========================================================================
*/
#ifndef SENSOR_H
#define SENSOR_H

//#include "ethernet_packet.h"
#include"json_client.h"

/*
**===========================================================================
**						--- macros ---
**===========================================================================
*/
#define OHT_L1                    GPIO_Pin_11

#define OHT_L2                    GPIO_Pin_12

#define OHT_L3                    GPIO_Pin_13

#define OHT_L4                    GPIO_Pin_14

#define OHT_L5                    GPIO_Pin_15

#define OHT_L6                    GPIO_Pin_8

#define OHT_L7                    GPIO_Pin_9

#define OHT_L8                    GPIO_Pin_15

#define OHT_L9                    GPIO_Pin_3

#define OHT_L10                   GPIO_Pin_1


#define OHT_PORT            		 GPIOB

#define OHT_CLOCK_PORT           RCC_AHB1Periph_GPIOB

#define	READ_OHT_L1				GPIO_ReadInputDataBit(GPIOB, OHT_L1)

#define	READ_OHT_L2				GPIO_ReadInputDataBit(GPIOB, OHT_L2)

#define	READ_OHT_L3				GPIO_ReadInputDataBit(GPIOB, OHT_L3)

#define	READ_OHT_L4				GPIO_ReadInputDataBit(GPIOB, OHT_L4)

#define	READ_OHT_L5				GPIO_ReadInputDataBit(GPIOB, OHT_L5)

#define	READ_OHT_L6				GPIO_ReadInputDataBit(GPIOA, OHT_L6)

#define	READ_OHT_L7				GPIO_ReadInputDataBit(GPIOA, OHT_L7)

#define	READ_OHT_L8				GPIO_ReadInputDataBit(GPIOA, OHT_L8)

#define	READ_OHT_L9				GPIO_ReadInputDataBit(GPIOB, OHT_L9)

#define	READ_OHT_L10			GPIO_ReadInputDataBit(GPIOB, OHT_L10)



#define UGT_L1                    GPIO_Pin_10

#define UGT_L2                    GPIO_Pin_11

#define UGT_L3                    GPIO_Pin_12

#define UGT_L4                    GPIO_Pin_13

#define UGT_L5                    GPIO_Pin_14

#define UGT_L6                    GPIO_Pin_15

#define UGT_PORT            		GPIOE

#define UGT_CLOCK_PORT            		RCC_AHB1Periph_GPIOE

#define	READ_UGT_L1				GPIO_ReadInputDataBit(OHT_PORT, UGT_L1)

#define	READ_UGT_L2				GPIO_ReadInputDataBit(OHT_PORT, UGT_L2)

#define	READ_UGT_L3 			GPIO_ReadInputDataBit(OHT_PORT, UGT_L3)

#define	READ_UGT_L4				GPIO_ReadInputDataBit(OHT_PORT, UGT_L4)

#define	READ_UGT_L5				GPIO_ReadInputDataBit(OHT_PORT, UGT_L5)

#define	READ_UGT_L6				GPIO_ReadInputDataBit(OHT_PORT, UGT_L6)


#define	READ_SENSOR_PORT		GPIO_ReadInputData(GPIOB);


#define INT8_PIN                	GPIO_Pin_7
#define INT8_PORT               	GPIOE
#define INT8_RCC               		RCC_AHB1Periph_GPIOE
#define INT8_EXTI_PortSource 		EXTI_PortSourceGPIOE
#define INT8_EXTI_PinSource         EXTI_PinSource7
#define INT8_EXTI_Line              EXTI_Line7
#define INT8_EXTI_IRQn              EXTI9_5_IRQn


#define INT7_PIN                    GPIO_Pin_8
#define INT7_PORT                   GPIOE
#define INT7_RCC                    RCC_AHB1Periph_GPIOE
#define INT7_EXTI_PortSource 		EXTI_PortSourceGPIOE
#define INT7_EXTI_PinSource         EXTI_PinSource8
#define INT7_EXTI_Line              EXTI_Line8
#define INT7_EXTI_IRQn              EXTI9_5_IRQn
									
#define INT2_PIN                    GPIO_Pin_2
#define INT2_PORT                   GPIOD
#define INT2_RCC                    RCC_AHB1Periph_GPIOD
#define INT2_EXTI_PortSource 		EXTI_PortSourceGPIOD
#define INT2_EXTI_PinSource         EXTI_PinSource2
#define INT2_EXTI_Line              EXTI_Line2
#define INT2_EXTI_IRQn              EXTI9_5_IRQn


#define SWITCH_PUMP_1							GPIO_Pin_11
#define SWITCH_PUMP_1_PORT						GPIOD
#define SWITCH_PUMP_1_RCC						RCC_AHB1Periph_GPIOD
#define SWITCH_PUMP_1_EXTI_PortSource			EXTI_PortSourceGPIOD
#define SWITCH_PUMP_1_EXTI_PinSource	    	EXTI_PinSource11
#define SWITCH_PUMP_1_EXTI_Line					EXTI_Line11
#define SWITCH_PUMP_1_EXTI_IRQn					EXTI15_10_IRQn

#define SWITCH_PUMP_2							GPIO_Pin_5
#define SWITCH_PUMP_2_PORT						GPIOB
#define SWITCH_PUMP_2_RCC						RCC_AHB1Periph_GPIOB
#define SWITCH_PUMP_2_EXTI_PortSource			EXTI_PortSourceGPIOB
#define SWITCH_PUMP_2_EXTI_PinSource	    	EXTI_PinSource5
#define SWITCH_PUMP_2_EXTI_Line					EXTI_Line5
#define SWITCH_PUMP_2_EXTI_IRQn					EXTI9_5_IRQn


extern uint32_t sensor_switch_input_bits_curr, sensor_switch_input_bits_prev;
uint8_t read_pin_status(void);
void sensor_inputs_init(void);
void power_sense_pin_init(void);
void reset_loc_flags(void);
void save_sensor_settings(struct json_struct *wms_payload_info, uint16_t num_bytes);
uint8_t send_tank_status_GUI(uint32_t *tank1_status, uint32_t tank2_status, uint8_t *send_arr);
//uint32_t get_oht_current_level(void);
//uint32_t get_ugt_current_level(void);
void read_tank_pump_settings(void);
uint32_t get_current_level(struct tank_info *tank_ptr);
void response_tank_pump_settings(uint8_t tank_num);

#endif
/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
